﻿using Ignite.Abacus.Mobile.Interfaces;
using Ignite.Abacus.Mobile.Models;
using Ignite.Abacus.Mobile.Objects;
using Mathos.Parser;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Ignite.Abacus.Mobile.Adapters
{
	/// <summary>
	/// Provides an implementation of <see cref="ICalculatorAdapter"/> that leverages
	/// the MathosParser library as it's calculation engine.
	/// </summary>
	public class LocalAdapter : ICalculatorAdapter
	{
		private readonly MathParser _parser;

		public LocalAdapter() 
		{ _parser = new MathParser (); }

		public string Name { get { return Resources.LocalAdapterName; } }

		public IEnumerable<ICalculatorButtonModel> GetAvailableButtons()
		{
			return new List<ICalculatorButtonModel> {
				new CalculatorButtonModel("sinh")		{ Row = 0, Column = 0, ActionType = ButtonActionType.Expression, Execute = s => "sinh(" + s + ")" },
				new CalculatorButtonModel("sin")		{ Row = 0, Column = 1, ActionType = ButtonActionType.Expression, Execute = s => "sin(" + s + ")" },
				new CalculatorButtonModel("cosh")		{ Row = 0, Column = 2, ActionType = ButtonActionType.Expression, Execute = s => "cosh(" + s + ")" },
				new CalculatorButtonModel("cos")		{ Row = 0, Column = 3, ActionType = ButtonActionType.Expression, Execute = s => "cos(" + s + ")" },

				new CalculatorButtonModel("tanh")		{ Row = 1, Column = 0, ActionType = ButtonActionType.Expression, Execute = s => "tanh(" + s + ")" },
				new CalculatorButtonModel("tan")		{ Row = 1, Column = 1, ActionType = ButtonActionType.Expression, Execute = s => "tan(" + s + ")" },
				new CalculatorButtonModel("x²")			{ Row = 1, Column = 2, ActionType = ButtonActionType.Expression, Execute = s => s + "^2" },
				new CalculatorButtonModel("xʸ")			{ Row = 1, Column = 3, ActionType = ButtonActionType.Operator,   DisplayText = "^" },

				new CalculatorButtonModel("x³")			{ Row = 2, Column = 0, ActionType = ButtonActionType.Expression, Execute = s => s + "^3" },
				new CalculatorButtonModel("10^x")		{ Row = 2, Column = 1, ActionType = ButtonActionType.Expression, Execute = s => "10^" + s },
				new CalculatorButtonModel("√x")			{ Row = 2, Column = 2, ActionType = ButtonActionType.Expression, Execute = s => "sqrt(" + s + ")" },
				new CalculatorButtonModel("π")			{ Row = 2, Column = 3, ActionType = ButtonActionType.Direct,	 DisplayText = "3.1415926535897932384626433832795" },

				new CalculatorButtonModel("±")			{ Row = 3, Column = 1, ColumnSpan = 2, ActionType = ButtonActionType.Inline,	 Execute = s => {
					if (string.IsNullOrEmpty(s)) return s;
					return s.StartsWith("-") ? s.Substring(1) : "-" + s;
				}}
			};
		}

		public async Task<String> PerformCalculation(string expression)
		{
			try
			{ return _parser.Parse(expression).ToString(); }
			catch
			{ return Resources.ErrorParsingEquation; }
		}
	}
}
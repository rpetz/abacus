﻿using Ignite.Abacus.Mobile.Interfaces;
using Ignite.Abacus.Mobile.Models;
using Ignite.Abacus.Mobile.Objects;
using Splat;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Ignite.Abacus.Mobile.Adapters
{
	/// <summary>
	/// Provides an implementation of <see cref="ICalculatorAdapter"/> that leverages
	/// the Calcatraz web service as it's calculation engine.
	/// </summary>
	public class CalcatrazAdapter : ICalculatorAdapter
	{
		private const String Url = "https://www.calcatraz.com/calculator/api?c={0}";
		private readonly IWebAccessService _webAccessService;

		public CalcatrazAdapter()
		{ _webAccessService = Locator.Current.GetService<IWebAccessService>(); }

		public string Name { get { return Resources.CalcatrazAdapterName; } }

		public IEnumerable<ICalculatorButtonModel> GetAvailableButtons()
		{
			return new List<ICalculatorButtonModel> {
				new CalculatorButtonModel("ln")			{ Row = 0, Column = 0, ActionType = ButtonActionType.Expression, Execute = s => "ln(" + s + ")" },
				new CalculatorButtonModel("π")			{ Row = 0, Column = 1, ActionType = ButtonActionType.Direct,	 DisplayText = "3.1415926535897932384626433832795" },
				new CalculatorButtonModel("√x")			{ Row = 0, Column = 2, ActionType = ButtonActionType.Expression, Execute = s => "sqrt(" + s + ")" },
				new CalculatorButtonModel("x²")			{ Row = 0, Column = 3, ActionType = ButtonActionType.Expression, Execute = s => s + "^2" },
				new CalculatorButtonModel("x³")			{ Row = 1, Column = 0, ActionType = ButtonActionType.Expression, Execute = s => s + "^3" },
				new CalculatorButtonModel("xʸ")			{ Row = 1, Column = 1, ActionType = ButtonActionType.Operator,   DisplayText = "^" },
				new CalculatorButtonModel("10^x")		{ Row = 1, Column = 2, ActionType = ButtonActionType.Expression, Execute = s => "10^" + s },
				new CalculatorButtonModel("±")			{ Row = 1, Column = 3, ActionType = ButtonActionType.Inline,	 Execute = s => {
					if (string.IsNullOrEmpty(s)) return s;					
					return s.StartsWith("-") ? s.Substring(1) : "-" + s;
				}}
			};
		}

		public async Task<String> PerformCalculation(string expression)
		{
			try
			{
				var url = String.Format(Url, Uri.EscapeDataString(expression));
				var responseBody = await _webAccessService.CreateWebRequest(url);
				var response = responseBody.Trim();
				if (String.IsNullOrWhiteSpace(response)) return "0";
				return response;
			}
			catch {
				return Resources.ErrorParsingEquation;
			}
		}
	}
}

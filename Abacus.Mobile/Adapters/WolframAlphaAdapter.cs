﻿using Ignite.Abacus.Mobile.Interfaces;
using Ignite.Abacus.Mobile.Models;
using Ignite.Abacus.Mobile.Objects;
using Splat;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Ignite.Abacus.Mobile.Adapters
{
	/// <summary>
	/// Provides an implementation of <see cref="ICalculatorAdapter"/> that leverages
	/// the Wolfram|Alpha web service as it's calculation engine.
	/// </summary>
	public class WolframAlphaAdapter : ICalculatorAdapter
	{
		private const String AppId = "32QHVL-R44RQRR958";
		private const String Url = "http://api.wolframalpha.com/v2/query?input={0}&appid={1}";
		private readonly IWebAccessService _webAccessService;

		public string Name { get { return Resources.WolframAlphaAdapterName; } }

		public WolframAlphaAdapter()
		{ _webAccessService = Locator.Current.GetService<IWebAccessService>(); }

		public IEnumerable<ICalculatorButtonModel> GetAvailableButtons()
		{
			return new List<ICalculatorButtonModel> {
				new CalculatorButtonModel("ln")			{ Row = 0, Column = 0, ActionType = ButtonActionType.Expression, Execute = s => "ln(" + s + ")" },
				new CalculatorButtonModel("sinh")		{ Row = 0, Column = 1, ActionType = ButtonActionType.Expression, Execute = s => "sinh(" + s + ")" },
				new CalculatorButtonModel("sin")		{ Row = 0, Column = 2, ActionType = ButtonActionType.Expression, Execute = s => "sin(" + s + ")" },
				new CalculatorButtonModel("cosh")		{ Row = 0, Column = 3, ActionType = ButtonActionType.Expression, Execute = s => "cosh(" + s + ")" },

				new CalculatorButtonModel("cos")		{ Row = 1, Column = 0, ActionType = ButtonActionType.Expression, Execute = s => "cos(" + s + ")" },
				new CalculatorButtonModel("tanh")		{ Row = 1, Column = 1, ActionType = ButtonActionType.Expression, Execute = s => "tanh(" + s + ")" },
				new CalculatorButtonModel("tan")		{ Row = 1, Column = 2, ActionType = ButtonActionType.Expression, Execute = s => "tan(" + s + ")" },
				new CalculatorButtonModel("x²")			{ Row = 1, Column = 3, ActionType = ButtonActionType.Expression, Execute = s => s + "^2" },

				new CalculatorButtonModel("x³")			{ Row = 2, Column = 0, ActionType = ButtonActionType.Expression, Execute = s => s + "^3" },
				new CalculatorButtonModel("xʸ")			{ Row = 2, Column = 1, ActionType = ButtonActionType.Operator,   DisplayText = "^" },
				new CalculatorButtonModel("10^x")		{ Row = 2, Column = 2, ActionType = ButtonActionType.Expression, Execute = s => "10^" + s },
				new CalculatorButtonModel("√x")			{ Row = 2, Column = 3, ActionType = ButtonActionType.Expression, Execute = s => "sqrt(" + s + ")" },

				new CalculatorButtonModel("∛x")			{ Row = 3, Column = 0, ActionType = ButtonActionType.Expression, Execute = s => "cuberoot(" + s + ")" },
				new CalculatorButtonModel("1/x")		{ Row = 3, Column = 1, ActionType = ButtonActionType.Expression, Execute = s => "reciproc(" + s + ")" },
				new CalculatorButtonModel("π")			{ Row = 3, Column = 2, ActionType = ButtonActionType.Direct,	 DisplayText = "3.1415926535897932384626433832795" },
				new CalculatorButtonModel("n!")			{ Row = 3, Column = 3, ActionType = ButtonActionType.Expression, Execute = s => "fact(" + s + ")" },

				new CalculatorButtonModel("±")			{ Row = 4, Column = 1, ColumnSpan = 2, ActionType = ButtonActionType.Inline,	 Execute = s => {
					if (string.IsNullOrEmpty(s)) return s;					
					return s.StartsWith("-") ? s.Substring(1) : "-" + s;
				}}
			};
		}

		public async Task<string> PerformCalculation(string expression)
		{
			try {
				var url = String.Format(Url, Uri.EscapeDataString(expression), AppId);
				var responseBody = await _webAccessService.CreateWebRequest(url);

				var root = XDocument.Parse(responseBody).Root;

				var success = Boolean.Parse(root.Attributes().FirstOrDefault(x => x.Name == "success").Value);
				var error = Boolean.Parse(root.Attributes().FirstOrDefault(x => x.Name == "error").Value);

				// I've yet to be able to get these to be caught...It's an unknown error at this moment.
				if (!success || error)
					return Resources.ErrorParsingEquation;

				var resultPodValue =
					(from x in root.Elements("pod")
					 where x.Attributes().FirstOrDefault(y => y.Name == "id").Value.Equals("result", StringComparison.OrdinalIgnoreCase)
					 select x.Elements().FirstOrDefault().Element("plaintext").Value).FirstOrDefault();
				var decimalApproxPodValue =
					(from x in root.Elements("pod")
					 where x.Attributes().FirstOrDefault(y => y.Name == "id").Value.Equals("decimalApproximation", StringComparison.OrdinalIgnoreCase)
					 select x.Elements().FirstOrDefault().Element("plaintext").Value).FirstOrDefault();

				if (decimalApproxPodValue != null) return decimalApproxPodValue;
				return resultPodValue;
			}
			catch
			{
				return Resources.ErrorParsingEquation;
			}
		}
	}
}
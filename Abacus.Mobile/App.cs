﻿using Ignite.Abacus.Mobile.Adapters;
using Ignite.Abacus.Mobile.Interfaces;
using Ignite.Abacus.Mobile.Services;
using Ignite.Abacus.Mobile.ViewModels;
using Splat;

namespace Ignite.Abacus.Mobile
{
	public class App
	{
		// We bind dependencies here so we aren't repeating the DI setup code across platform-specific projects.
		public static void BindDependencies()
		{
			// Bind calculator adapters.
			Locator.CurrentMutable.Register (() => new LocalAdapter (), typeof(ICalculatorAdapter));
			Locator.CurrentMutable.Register (() => new WolframAlphaAdapter (), typeof(ICalculatorAdapter));
			Locator.CurrentMutable.Register (() => new CalcatrazAdapter (), typeof(ICalculatorAdapter));

			// Bind supporting services.
			Locator.CurrentMutable.Register (() => new ButtonGeneratorService (), typeof(IButtonGeneratorService));
			Locator.CurrentMutable.Register (() => new WebAccessService (), typeof(IWebAccessService));

			// Bind Viewmodels.
			Locator.CurrentMutable.Register (() => new CalculatorViewModel(), typeof(ICalculatorViewModel));
			Locator.CurrentMutable.Register (() => new OptionsViewModel (), typeof(OptionsViewModel));
		}
	}
}
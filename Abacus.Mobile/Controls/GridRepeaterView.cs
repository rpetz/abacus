﻿using System;
using System.Linq;
using Ignite.Abacus.Mobile.Interfaces;
using Xamarin.Forms; 
using System.Collections.Specialized;
using ReactiveUI;

namespace Ignite.Abacus.Mobile.Controls
{
	/// <summary>
	/// This is merged version of RepeaterView and 
	/// @Rachel's GridHelper class ported to Xamarin.Forms.
	/// Credit for GridHelper goes to @Rachel:
	/// Source: http://rachel53461.wordpress.com/2011/09/17/wpf-grids-rowcolumn-count-properties/
	/// </summary>
	public class GridRepeaterView<T> : Grid
	{
		public static readonly BindableProperty ItemsSourceProperty = BindableProperty.Create((GridRepeaterView<T> p) => p.ItemsSource, new ReactiveList<T> (), BindingMode.OneWay, null, ItemsChanged);

		public static readonly BindableProperty ItemTemplateProperty = BindableProperty.Create((GridRepeaterView<T> p) => p.ItemTemplate, null);

		public static readonly BindableProperty RowCountProperty = BindableProperty.Create((GridRepeaterView<T> p) => p.RowCount, default(int), BindingMode.OneWay, propertyChanged: RowCountChanged);

		public static readonly BindableProperty ColumnCountProperty = BindableProperty.Create((GridRepeaterView<T> p) => p.ColumnCount, default(int), BindingMode.OneWay, propertyChanged: ColumnCountChanged);

		public static readonly BindableProperty StarRowsProperty = BindableProperty.Create((GridRepeaterView<T> p) => p.StarRows, default(String), BindingMode.OneWay, propertyChanged: StarRowsChanged);

		public static readonly BindableProperty StarColumnsProperty = BindableProperty.Create((GridRepeaterView<T> p) => p.StarColumns, default(String), BindingMode.OneWay, propertyChanged: StarColumnsChanged);

		public ReactiveList<T> ItemsSource {
			get { return (ReactiveList<T>)GetValue (ItemsSourceProperty); }
			set { SetValue (ItemsSourceProperty, value); }
		}

		public DataTemplate ItemTemplate {
			get { return (DataTemplate)GetValue (ItemTemplateProperty); }
			set { SetValue (ItemTemplateProperty, value); }
		}

		public int RowCount {
			get { return (int)GetValue (RowCountProperty); }
			set { SetValue (RowCountProperty, value); }
		}

		private static void RowCountChanged (BindableObject bindable, int oldValue, int newValue)
		{
			if (!(bindable is GridRepeaterView<T>) || newValue < 0)
				return;

			var grid = (GridRepeaterView<T>)bindable;
			grid.RowDefinitions.Clear();

			for (var i = 0; i < newValue; i++)
				grid.RowDefinitions.Add(new RowDefinition { Height = GridLength.Auto });

			grid.SetStarRows(grid);
		}

		public int ColumnCount {
			get { return (int)GetValue (ColumnCountProperty); }
			set { SetValue (ColumnCountProperty, value); }
		}

		private static void ColumnCountChanged (BindableObject bindable, int oldValue, int newValue)
		{
			if (!(bindable is GridRepeaterView<T>) || newValue < 0)
				return;

			var grid = (GridRepeaterView<T>)bindable;
			grid.ColumnDefinitions.Clear();

			for (var i = 0; i < newValue; i++)
				grid.ColumnDefinitions.Add(new ColumnDefinition { Width = GridLength.Auto });

			grid.SetStarColumns(grid);
		}

		public String StarRows {
			get { return (string)GetValue (StarRowsProperty); }
			set { SetValue (StarRowsProperty, value); }
		}

		private static void StarRowsChanged (BindableObject bindable, String oldValue, String newValue)
		{
			if (!(bindable is GridRepeaterView<T>) || string.IsNullOrEmpty(newValue))
				return;

			((GridRepeaterView<T>)bindable).SetStarRows((GridRepeaterView<T>)bindable);
		}

		public String StarColumns {
			get { return (string)GetValue (StarColumnsProperty); }
			set { SetValue (StarColumnsProperty, value); }
		}

		private static void StarColumnsChanged (BindableObject bindable, String oldValue, String newValue)
		{
			if (!(bindable is GridRepeaterView<T>) || string.IsNullOrEmpty(newValue))
				return;

			((GridRepeaterView<T>)bindable).SetStarColumns((GridRepeaterView<T>)bindable);
		}

		private static void ItemsChanged (BindableObject bindable, ReactiveList<T> oldValue, ReactiveList<T> newValue)
		{
			var repeaterView = bindable as GridRepeaterView<T>;
			repeaterView.ItemsSource.CollectionChanged += repeaterView.ItemsSource_CollectionChanged;
			repeaterView.Children.Clear ();
			foreach (T item in newValue) {
				var obj = repeaterView.ItemTemplate.CreateContent ();
				var view = ((ViewCell)obj).View;
				view.BindingContext = item;
				SetCalculatorValues (view, item);
				repeaterView.Children.Add (((ViewCell)obj).View);
			}
		}

		private void SetStarColumns(Grid grid) 
		{
			var value = (String)GetValue (StarColumnsProperty);
			var starColumns = value.Split(',');
			var starAllColumns = value.ToLower() == "all";

			for (var i = 0; i < grid.ColumnDefinitions.Count; i++)
			{
				if (starAllColumns || starColumns.ToList().Contains(i.ToString()))
					grid.ColumnDefinitions[i].Width = new GridLength(1, GridUnitType.Star);
			}
		}

		private void SetStarRows(Grid grid) 
		{
			var value = (String)GetValue (StarRowsProperty);
			var starRows = value.Split(',');
			var starAllRows = value.ToLower() == "all";

			for (var i = 0; i < grid.RowDefinitions.Count; i++)
			{
				if (starAllRows || starRows.Contains(i.ToString()))
					grid.RowDefinitions[i].Height = new GridLength(1, GridUnitType.Star);
			}
		}

		//HACK: Since style property setters are not yet supported in Xamarin.Forms this
		//temporarily provides access to setting the row and column for each item's view
		private static void SetCalculatorValues(BindableObject view, object item) {
			if (!(item is ICalculatorButtonModel))
				return;
			var btn = (ICalculatorButtonModel)item;
			SetRow(view, btn.Row);
			SetColumn(view, btn.Column);
			SetRowSpan(view, btn.RowSpan);
			SetColumnSpan(view, btn.ColumnSpan);
		}

		private void ItemsSource_CollectionChanged (object sender, NotifyCollectionChangedEventArgs e)
		{
			if (e.OldItems != null) {
				Children.RemoveAt (e.OldStartingIndex);
				UpdateChildrenLayout ();
				InvalidateLayout ();
			}
			if (e.NewItems != null) {
				var enumerator = e.NewItems.GetEnumerator ();
				try {
					while (enumerator.MoveNext ()) {
						var t = (T)enumerator.Current;
						var obj = ItemTemplate.CreateContent ();
						var view = ((ViewCell)obj).View;
						view.BindingContext = t;
						SetCalculatorValues(view, t);
						Children.Insert (ItemsSource.IndexOf (t), view);
					}
				}
				finally {
					var disposable = enumerator as IDisposable;
					if (disposable != null) {
						disposable.Dispose ();
					}
				}
				UpdateChildrenLayout ();
				InvalidateLayout ();
			}
		}
	}
}
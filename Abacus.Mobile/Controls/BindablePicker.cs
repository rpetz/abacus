﻿using ReactiveUI;
using System;
using System.Collections;
using System.Collections.Specialized;
using Xamarin.Forms;
using Xamarin.Forms.Labs.Controls;

namespace Ignite.Abacus.Mobile.Controls
{
	// This class is directly from the latest code on the Xamarin.Forms.Labs Github.
	// I pulled this in directly because I couldn't get a ValueConverter working on the default Xamarin Picker view,
	// it would get constructed but would never call 'Convert'.
	// TODO: Remove this when possible
	public class BindablePicker : View
	{
		public BindablePicker()
		{
			Items = new ReactiveList<string>();
			Items.CollectionChanged += OnItemsCollectionChanged;
			SelectedIndexChanged += OnSelectedIndexChanged;
		}

		public Func<object, string> SourceItemLabelConverter { get; set; }

		public static readonly BindableProperty ItemsSourceProperty =
			BindableProperty.Create<BindablePicker, IList>(o => o.ItemsSource, default(IList), propertyChanged: OnItemsSourceChanged);

		public static readonly BindableProperty SelectedItemProperty =
			BindableProperty.Create<BindablePicker, object>(o => o.SelectedItem, default(object), BindingMode.TwoWay, propertyChanged: OnSelectedItemChanged);

		public IList ItemsSource
		{
			get { return (IList)GetValue(ItemsSourceProperty); }
			set { SetValue(ItemsSourceProperty, value); }
		}

		public object SelectedItem
		{
			get { return GetValue(SelectedItemProperty); }
			set { SetValue(SelectedItemProperty, value); }
		}

		private static void OnItemsSourceChanged(BindableObject bindable, IList oldvalue, IList newvalue)
		{
			var picker = bindable as BindablePicker;
			picker.Items.Clear();
			if (newvalue == null)
				return;
			
			foreach (var item in newvalue)
				picker.Items.Add(picker.SourceItemLabelConverter != null ? picker.SourceItemLabelConverter(item) : item.ToString());
		}

		private void OnSelectedIndexChanged(object sender, EventArgs eventArgs)
		{
			if (SelectedIndex < 0 || SelectedIndex > Items.Count - 1)
				SelectedItem = null;
			else
				SelectedItem = ItemsSource[SelectedIndex];
		}

		private static void OnSelectedItemChanged(BindableObject bindable, object oldvalue, object newvalue)
		{
			var picker = bindable as BindablePicker;
			if (newvalue != null) {
				var title = picker.SourceItemLabelConverter != null ? picker.SourceItemLabelConverter (newvalue) : newvalue.ToString ();
				picker.SelectedIndex = picker.Items.IndexOf (title);
			} else {
				picker.SelectedIndex = -1;
			}
		}

		public static readonly BindableProperty TitleProperty = BindableProperty.Create((BindablePicker w) => w.Title, null);
		public static readonly BindableProperty SelectedIndexProperty = BindableProperty.Create((BindablePicker w) => w.SelectedIndex, -1, BindingMode.TwoWay, null, delegate(BindableObject bindable, int oldvalue, int newvalue)
			{
				var selectedIndexChanged = ((BindablePicker)bindable).SelectedIndexChanged;
				if (selectedIndexChanged != null)
					selectedIndexChanged(bindable, EventArgs.Empty);
			}, null, CoerceSelectedIndex);

		public event EventHandler SelectedIndexChanged;

		private static int CoerceSelectedIndex(BindableObject bindable, int value)
		{
			var picker = (BindablePicker)bindable;
			if (picker.Items != null)
				return Math.Min (picker.Items.Count - 1, Math.Max (value, -1));
			return -1;
		}

		public string Title {
			get { return (string)GetValue(TitleProperty); }
			set { SetValue(TitleProperty, value); }
		}

		public ReactiveList<string> Items { get; private set; }

		public int SelectedIndex {
			get { return (int)GetValue(SelectedIndexProperty); }
			set { SetValue(SelectedIndexProperty, value); }
		}

		private void OnItemsCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
		{
			if (SelectedItem == null) 
				return;

			var title = SourceItemLabelConverter != null ? SourceItemLabelConverter (SelectedItem) : SelectedItem.ToString ();
			SelectedIndex = Items.IndexOf (title);
		}

		/// <summary>
		/// The HasBorder property
		/// </summary>
		public static readonly BindableProperty HasBorderProperty = BindableProperty.Create("HasBorder", typeof(bool), typeof(ExtendedEntry), true);

		/// <summary>
		/// Gets or sets if the border should be shown or not
		/// </summary>
		public bool HasBorder {
			get { return (bool)GetValue(HasBorderProperty); }
			set { SetValue(HasBorderProperty, value); }
		}	
	}
}


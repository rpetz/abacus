﻿using System.Runtime.Serialization;

namespace Ignite.Abacus.Mobile.Objects
{
	/// <summary>
	/// Defines the calculator keyboad style to display.
	/// </summary>
	public enum KeypadStyle
	{
		/// <summary>
		/// Represents a standard calculator keypad.
		/// 9 is in the top right, 1 bottom left.
		/// </summary>
		[EnumMember(Value = "Calculator Keypad")]
		Calculator,
		/// <summary>
		/// Represents a standard phone keypad.
		/// 9 is in the bottom right, 1 top left.
		/// </summary>
		[EnumMember(Value = "Phone Keypad")]
		Phone
	}
}
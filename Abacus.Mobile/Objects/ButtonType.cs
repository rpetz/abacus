﻿namespace Ignite.Abacus.Mobile.Objects
{
	/// <summary>
	/// Defines the available button types on the keypad.
	/// </summary>
	public enum ButtonType
	{
		/// <summary>
		/// Represents a standard input button such as 0-9, Multiply, or x^y.
		/// </summary>
		Input,
		/// <summary>
		/// Represents an 'equals' button.
		/// </summary>
		Equals,
		/// <summary>
		/// Represents a 'clear' button.
		/// </summary>
		Clear
	}
}
﻿namespace Ignite.Abacus.Mobile.Objects
{
	/// <summary>
	/// Defines the possible locations of the adapter buttons.
	/// </summary>
	public enum AdapterButtonsLocation
	{
		/// <summary>
		/// Places adapter buttons below the standard buttons.
		/// </summary>
		Bottom,
		/// <summary>
		/// Places adapter buttons to the right side of the standard buttons.
		/// </summary>
		Right,
		/// <summary>
		/// Places adapter buttons to the left side of the standard buttons.
		/// </summary>
		Left
	}
}
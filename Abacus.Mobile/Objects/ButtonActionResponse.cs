﻿namespace Ignite.Abacus.Mobile.Objects
{
	/// <summary>
	/// Defines how to proceed after a button is pressed.
	/// </summary>
	public enum ButtonActionResponse
	{
		/// <summary>
		/// Responds that an action does not require further interaction.
		/// </summary>
		Completed,
		/// <summary>
		/// Responds that an action requires further interaction.
		/// </summary>
		RequiresMoreInteraction,
	}
}
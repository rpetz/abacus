﻿namespace Ignite.Abacus.Mobile.Objects
{
	/// <summary>
	/// Defines the type of action the button performs.
	/// </summary>
	public enum ButtonActionType
	{
		/// <summary>
		/// Represents an operator, such as multiply or divide.
		/// </summary>
		Operator,
		/// <summary>
		/// Represents a button that creates an expression, such as 'reciproc(x)'
		/// </summary>
		Expression,
		/// <summary>
		/// Represents a direct input button, such as the numeric keys.
		/// </summary>
		Direct,
		/// <summary>
		/// Represents an inline button, such as the 'sign inversion' button.
		/// </summary>
		Inline
	}
}
﻿using System.Collections.Generic;
using Ignite.Abacus.Mobile.Objects;

namespace Ignite.Abacus.Mobile.Interfaces
{
	/// <summary>
	/// Defines a service for generating buttons for a <see cref="ICalculatorViewModel"/>
	/// </summary>
	public interface IButtonGeneratorService
	{
		/// <summary>
		/// Generates a collection of buttons based upon the provided <see cref="KeypadStyle"/>,
		/// <see cref="AdapterButtonsLocation"/>, and collection of <see cref="ICalculatorButtonModel"/>s.
		/// </summary>
		/// <param name="keypadStyle">The keypad style to lay the buttons out with</param>
		/// <param name="adapterButtonsLocation">The location of where to place the adapter buttons</param>
		/// <param name="adapterButtons">The adapter buttons to include in the resulting collection</param>
		/// <returns>A collection of buttons mapped to grid style coordinates</returns>
		IEnumerable<ICalculatorButtonModel> GenerateButtonGrid(KeypadStyle keypadStyle, AdapterButtonsLocation adapterButtonsLocation, IEnumerable<ICalculatorButtonModel> adapterButtons);
	}
}
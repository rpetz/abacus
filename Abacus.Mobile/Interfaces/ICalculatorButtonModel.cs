﻿using System;
using Ignite.Abacus.Mobile.Objects;

namespace Ignite.Abacus.Mobile.Interfaces
{
	/// <summary>
	/// Defines a model for representing a calculator button.
	/// </summary>
	public interface ICalculatorButtonModel
	{
		/// <summary>
		/// Provides a tie back into the ViewModel that created the button.
		/// </summary>
		ICalculatorViewModel ViewModel { get; set; }

		/// <summary>
		/// Provides the button's type - such as 'Clear', 'Equals', or 'Action'.
		/// </summary>
		ButtonType ButtonType { get; }

		/// <summary>
		/// Provides the button's action type
		/// </summary>
		ButtonActionType ActionType { get; }

		/// <summary>
		/// Provides the title text for the button.
		/// </summary>
		string TitleText { get; }

		/// <summary>
		/// Provides the text that the button will display on the calculator screen.
		/// </summary>
		string DisplayText { get; }

		/// <summary>
		/// Provides the column that the button should be placed in.
		/// </summary>
		int Column { get; set; }

		/// <summary>
		/// Provides the row that the button should be placed in.
		/// </summary>
		int Row { get; set; }

		/// <summary>
		/// Provides the number of columns that the button should take up.
		/// </summary>
		int ColumnSpan { get; set; }

		/// <summary>
		/// Provides the number of rows that the button should take up.
		/// </summary>
		int RowSpan { get; set; }

		/// <summary>
		/// Provides an action to retrieve the result of the button.
		/// </summary>
		Func<String, String> Execute { get; set; }
	}
}
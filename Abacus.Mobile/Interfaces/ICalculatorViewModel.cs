﻿using System;
using Ignite.Abacus.Mobile.Objects;
using ReactiveUI;

namespace Ignite.Abacus.Mobile.Interfaces
{
	/// <summary>
	/// Defines a view model for representing a calculator view.
	/// </summary>
	public interface ICalculatorViewModel
	{
		#region Services
		/// <summary>
		/// Exposes a calculator adapter that can be changed at runtime.
		/// </summary>
		ICalculatorAdapter CalculatorAdapter { get; set; }
		#endregion

		#region Commands
		/// <summary>
		/// Executes an action to handle a calculator button press.
		/// </summary>
		IReactiveCommand CalculatorButtonCommand { get; set; }

		/// <summary>
		/// Executes an action to navigate to the options view.
		/// </summary>
		ReactiveCommand<Object> GoToOptionsCommand { get; set; }

		/// <summary>
		/// Executes an action to set the keypad style to use.
		/// </summary>
		ReactiveCommand<Object> SetKeypadStyleCommand { get; set; }

		/// <summary>
		/// Executes an action to set the location of adapter buttons in relation to the main keypad.
		/// </summary>
		ReactiveCommand<Object> SetAdapterButtonsLocationCommand { get; set; }

		/// <summary>
		/// Executes an action to set the current calculator adapter.
		/// </summary>
		ReactiveCommand<Object> SetCalculatorAdapterCommand { get; set; }
		#endregion

		#region Properties
		/// <summary>
		/// Creates an observable source for the currently available calulator buttons.
		/// Contains both the standard keypad button set and the currently selected
		/// calculator adapter's available button set.
		/// </summary>
		ReactiveList<ICalculatorButtonModel> Buttons { get; set; }

		/// <summary>
		/// Displays the current calculator equation chain.
		/// </summary>
		String EquationText { get; set; }

		/// <summary>
		/// Displays the current text based upon the current calculator action.
		/// For example: Ternary actions may 'cache' in the display text buffer
		/// before they are passed to the Equation property if they have not yet
		/// been fully evaluated.
		/// </summary>
		String BufferText { get; set; }

		/// <summary>
		/// Provides an enum representing where to display adapter buttons in relation
		/// to the main keypad buttons (left, bottom, or right).
		/// </summary>
		AdapterButtonsLocation AdapterButtonsLocation { get; set; }

		/// <summary>
		/// Provides an enum representing the keypad style to use for the main keypad 
		/// buttons (phone keypad style vs. calculator keypad style).
		/// </summary>
		KeypadStyle KeypadStyle { get; set; }

		/// <summary>
		/// Exposes the current number of button rows to display.
		/// </summary>
		int ButtonRowCount { get; set; }

		/// <summary>
		/// Exposes the current number of button columns to display.
		/// </summary>
		int ButtonColumnCount { get; set; }

		/// <summary>
		/// Exposes the available calculator adapters.
		/// </summary>
		ReactiveList<ICalculatorAdapter> AvailableAdapters { get; set; } 
		#endregion
	}
}
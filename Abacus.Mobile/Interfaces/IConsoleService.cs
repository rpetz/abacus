﻿namespace Ignite.Abacus.Mobile.Interfaces
{
	public interface IConsoleService
	{
		/// <summary>
		/// Writes a line to the application output.
		/// </summary>
		/// <param name="value">The value to write</param>
		/// <param name="replacements">The replacements to inject into the value</param>
		void WriteLine(string value, params object[] replacements);
		/// <summary>
		/// Writes a line to the application output.
		/// </summary>
		/// <param name="value">The value to write</param>
		void WriteLine(object value);

		/// <summary>
		/// Writes to the application output.
		/// </summary>
		/// <param name="value">The value to write</param>
		/// <param name="replacements">The replacements to inject into the value</param>
		void Write(string value, params object[] replacements);

		/// <summary>
		/// Writes to the application output.
		/// </summary>
		/// <param name="value">The value to write</param>
		void Write(object value);
	}
}
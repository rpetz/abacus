﻿using System.Threading.Tasks;

namespace Ignite.Abacus.Mobile.Interfaces
{
	/// <summary>
	/// Defines a service that can connect to a remote URL and retrieve data.
	/// </summary>
	public interface IWebAccessService
	{
		/// <summary>
		/// Calls a remote URL and returns response data from it.
		/// </summary>
		/// <param name="url">The URL to invoke</param>
		/// <returns>A task to resolve the response data retrieved from the remote call</returns>
		Task<string> CreateWebRequest(string url);
	}
}
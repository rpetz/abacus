﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Ignite.Abacus.Mobile.Interfaces
{
	/// <summary>
	/// Defines a service for performing mathematical equation calculation
	/// </summary>
	public interface ICalculatorAdapter
	{
		/// <summary>
		/// Provides the name of the calculator adapter.
		/// </summary>
		string Name { get; }

		/// <summary>
		/// Provides a collection of buttons supported by this adapter.
		/// </summary>
		/// <returns></returns>
		IEnumerable<ICalculatorButtonModel> GetAvailableButtons();
		
		/// <summary>
		/// Performs a calculation on the provided mathematical expression.
		/// </summary>
		/// <param name="expression">The expression to evaluate</param>
		/// <returns>The result of the evaluated expression</returns>
		Task<String> PerformCalculation(string expression);
	}
}
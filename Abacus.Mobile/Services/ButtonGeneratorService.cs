﻿using System.Collections.Generic;
using System.Linq;
using Ignite.Abacus.Mobile.Interfaces;
using Ignite.Abacus.Mobile.Models;
using Ignite.Abacus.Mobile.Objects;

namespace Ignite.Abacus.Mobile.Services
{
	/// <summary>
	/// Provides a standard implementation of the <see cref="IButtonGeneratorService"/> interface.
	/// </summary>
	public class ButtonGeneratorService : IButtonGeneratorService
	{
		/// <summary>
		/// Generates a new collection of buttons laid out on a grid of rows and columns.
		/// </summary>
		/// <param name="keypadStyle">The style of keypad to generate</param>
		/// <param name="adapterButtonsLocation">Where to place the adapter buttons in relation to the standard keypad buttons</param>
		/// <param name="adapterButtons">The adapter buttons to add to the result set</param>
		/// <returns>A collection of buttons</returns>
		public IEnumerable<ICalculatorButtonModel> GenerateButtonGrid(KeypadStyle keypadStyle, AdapterButtonsLocation adapterButtonsLocation, IEnumerable<ICalculatorButtonModel> adapterButtons)
		{
			// First build the standard keypad buttons.
			var buttons = BuildStandardButtons(keypadStyle).ToList();

			// If there are no adapter buttons, just return the original button set.
			if (adapterButtons == null || !adapterButtons.Any())
				return buttons;

			// Next take the adapter buttons and place them into the area defined by the provided AdapterButtonsLocation.

			if (adapterButtonsLocation == AdapterButtonsLocation.Left)
			{
				// If the position is on the left - we move the standard buttons.
				var maxColumns = adapterButtons.Max(x => x.Column + x.ColumnSpan);

				buttons = buttons.Select(x => {
					x.Column += maxColumns;
					return x;
				}).ToList();
			}
			else
			{
				// If the position is on the bottom or on the right - we move the adapter buttons.
				var maxColumns = buttons.Max(x => x.Column + x.ColumnSpan);
				var maxRows = buttons.Max(x => x.Row + x.RowSpan);

				adapterButtons = adapterButtons.Select(x => {
					// If the position is on the right - move the adapter buttons over to the right.
					if (adapterButtonsLocation == AdapterButtonsLocation.Right) x.Column += maxColumns;
					// If the position is on the bottom - move the adapter buttons to the bottom.
					else if (adapterButtonsLocation == AdapterButtonsLocation.Bottom) x.Row += maxRows;
					return x;
				});
			}

			buttons.AddRange(adapterButtons);

			return buttons;
		}

		private IEnumerable<ICalculatorButtonModel> BuildStandardButtons(KeypadStyle keypadStyle)
		{
			// First build the static buttons.
			var buttons = new List<CalculatorButtonModel> {
				new CalculatorButtonModel("0") { Row = 4, Column = 0, ActionType = ButtonActionType.Direct },
				new CalculatorButtonModel(".") { Row = 4, Column = 1, ActionType = ButtonActionType.Direct },
				new CalculatorButtonModel("Clear") { Row = 0, Column = 0, ButtonType = ButtonType.Clear },
				new CalculatorButtonModel("=") { Row = 4, Column = 2, ColumnSpan = 2, ButtonType = ButtonType.Equals },
				new CalculatorButtonModel("(") { Row = 0, Column = 1, ActionType = ButtonActionType.Operator },
				new CalculatorButtonModel(")") { Row = 0, Column = 2, ActionType = ButtonActionType.Operator },
				new CalculatorButtonModel("/") { Row = 0, Column = 3, ActionType = ButtonActionType.Operator },
				new CalculatorButtonModel("*") { Row = 1, Column = 3, ActionType = ButtonActionType.Operator },
				new CalculatorButtonModel("-") { Row = 2, Column = 3, ActionType = ButtonActionType.Operator },
				new CalculatorButtonModel("+") { Row = 3, Column = 3, ActionType = ButtonActionType.Operator }
			};

			// Next build the numeric buttons.
			// We do this dynamically because their position change based upon the provided KeypadStyle

			// We need to split the buttons into their rows - 3 buttons per row.
			var numericButtonRows = Enumerable.Range(1, 9).SplitIntoBlocks(3);

			// Then we need to order those rows according to the button order.
			if (keypadStyle == KeypadStyle.Calculator) numericButtonRows = numericButtonRows.OrderByDescending(x => x.FirstOrDefault());

			// Then we need to project the collection into a flat list.
			// Rather than iterating through it again later, just select out the button itself.
			var row = 0;
			var numericButtons = numericButtonRows.SelectMany(x =>
			{
				var column = 0;
				var result = x.Select(y =>
				{
					var button = new CalculatorButtonModel(y.ToString()) { Row = row, Column = column, ActionType = ButtonActionType.Direct };
					column++;
					return button;
				});
				row++;
				return result;
			});

			// Add the numeric buttons into the collection.
			buttons.AddRange(numericButtons);

			return buttons;
		} 
	}
}
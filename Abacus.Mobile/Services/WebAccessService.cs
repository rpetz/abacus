﻿using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using Ignite.Abacus.Mobile.Interfaces;

namespace Ignite.Abacus.Mobile.Services
{
	/// <summary>
	/// Provides a standard implementation of the <see cref="IWebAccessService"/> interface.
	/// </summary>
	public class WebAccessService : IWebAccessService
	{
		/// <summary>
		/// Connects to a remote URL and returns the response body.
		/// </summary>
		/// <param name="url">The URL to connect to</param>
		/// <returns>The response body from the request</returns>
		public async Task<String> CreateWebRequest(string url)
		{
			// Create a WebRequest off of the provided URL.
			var wc = WebRequest.Create(url);
			// Run this request synchronously.
			var response = await wc.GetResponseAsync();
			// Read the response stream out.
			return new StreamReader(response.GetResponseStream()).ReadToEnd();
		}
	}
}
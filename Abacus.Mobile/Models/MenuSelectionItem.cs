﻿using System;

namespace Ignite.Abacus.Mobile.Models
{
	/// <summary>
	/// Represents a model for use in a WPF Menu control.
	/// </summary>
	public class MenuSelectionItem
	{
		/// <summary>
		/// The value of the object represented by this menu item.
		/// </summary>
		public object Value { get; set; }

		/// <summary>
		/// The text to display on the menu item.
		/// </summary>
		public String DisplayName { get; set; }

		/// <summary>
		/// Denotes whether or not this item is currently selected.
		/// </summary>
		public Boolean Selected { get; set; }

		/// <summary>
		/// Constructs a new instance of a MenuSelectionItem.
		/// </summary>
		/// <param name="value">The value to store in this item</param>
		/// <param name="displayName">The text to display on the item</param>
		/// <param name="selected">Whether or not the item is currently selected</param>
		public MenuSelectionItem(object value, string displayName, bool selected)
		{
			Value = value;
			DisplayName = displayName;
			Selected = selected;
		}
	}
}
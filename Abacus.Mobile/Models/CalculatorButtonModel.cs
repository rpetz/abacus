﻿using System;
using Ignite.Abacus.Mobile.Interfaces;
using Ignite.Abacus.Mobile.Objects;
using Splat;

namespace Ignite.Abacus.Mobile.Models
{
	/// <summary>
	/// Provides a standard implementation of the <see cref="ICalculatorButtonModel"/> interface.
	/// </summary>
	public class CalculatorButtonModel : ICalculatorButtonModel
	{
#region Private Properties
		private int _columnSpan = 1;
		private int _rowSpan = 1;
		private ICalculatorViewModel _viewModel;
#endregion

#region Constructors
		/// <summary>
		/// Creates a new instance of a calculator button
		/// </summary>
		/// <param name="value">The value to output and display</param>
		/// <param name="specialty">The specialty type of the button</param>
		public CalculatorButtonModel(String value, ButtonType specialty = ButtonType.Input)
		{
			DisplayText = value;
			TitleText = value;
			ButtonType = specialty;
			Execute = s => DisplayText;
		}

		/// <summary>
		/// Creates a new instance of a calculator button
		/// </summary>
		/// <param name="titleText">The value to display on the button</param>
		/// <param name="displayText">The value to display on the calculator screen</param>
		/// <param name="specialty">The specialty type of the button</param>
		public CalculatorButtonModel(String titleText, String displayText, ButtonType specialty = ButtonType.Input)
		{
			DisplayText = displayText;
			TitleText = titleText;
			ButtonType = specialty;
			Execute = s => DisplayText;
		}
#endregion

#region Properties
		public ButtonActionType ActionType { get; set; }
		public ButtonType ButtonType { get; set; }
		public string TitleText { get; set; }
		public string DisplayText { get; set; }
		public Func<string, string> Execute { get; set; }

		// Column, Row, ColumnSpan, and RowSpan should be set through property initialization.
		// (so as to reduce the number of constructor overloads).

		public int Column { get; set; }
		public int Row { get; set; }

		// Not an auto-property so we can set it to a default of 1 while supporting property initialization.
		public int ColumnSpan
		{
			get { return _columnSpan; }
			set { _columnSpan = value; }
		}

		// Not an auto-property so we can set it to a default of 1 while supporting property initialization.
		public int RowSpan
		{
			get { return _rowSpan; }
			set { _rowSpan = value; }
		}

		public ICalculatorViewModel ViewModel {
			get { 
				// HACK: Since we are setting the view model directly in the BindableGridView, we can't rely on
				// RxUI's nice DataTemplate bindings...Thus we need to forcibly set the CalculatorVM here so that
				// button's can call back to it's command.
				if (_viewModel == null)
					_viewModel = Locator.Current.GetService<ICalculatorViewModel> ();
				return _viewModel; }
			set { _viewModel = value; }
		}
#endregion
	}
}
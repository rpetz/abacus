﻿using Ignite.Abacus.Mobile.ViewModels;
using Xamarin.Forms;
using ReactiveUI;
using ReactiveUI.XamForms;

namespace Ignite.Abacus.Mobile
{	
	public partial class CalculatorView : ReactiveContentPage<CalculatorViewModel>
	{	
		public static readonly BindableProperty OptionsItemProperty =
			BindableProperty.Create<CalculatorView, ToolbarItem>(x => x.OptionsItem, default(ToolbarItem));

		public ToolbarItem OptionsItem {
			get { return (ToolbarItem)GetValue (OptionsItemProperty); }
			set { SetValue (OptionsItemProperty, value); }
		}

		public CalculatorView ()
		{
			InitializeComponent ();

			this.OneWayBind (ViewModel, x => x.Buttons, x => x.CalcBtnGrid.ItemsSource);
			this.Bind (ViewModel, x => x.ButtonRowCount, x => x.CalcBtnGrid.RowCount);
			this.Bind (ViewModel, x => x.ButtonColumnCount, x => x.CalcBtnGrid.ColumnCount);

			this.Bind (ViewModel, x => x.EquationText, x => x.EquationLbl.Text);
			this.Bind (ViewModel, x => x.BufferText, x => x.BufferLbl.Text);
			this.Bind (ViewModel, x => x.CalculatorAdapter.Name, x => x.SelectedAdapterLbl.Text);
			this.BindCommand (ViewModel, x => x.GoToOptionsCommand, x => x.OptionsItem);

			OptionsItem = new ToolbarItem () {
				Name = "Options"
			};

			AddToolbarItem ();
		}

		private void AddToolbarItem()
		{
			if (ToolbarItems.Count == 0)
				ToolbarItems.Add (OptionsItem);
		}
	}
}


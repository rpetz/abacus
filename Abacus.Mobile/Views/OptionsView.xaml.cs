﻿using System;
using Ignite.Abacus.Mobile.Interfaces;
using Ignite.Abacus.Mobile.Objects;
using Ignite.Abacus.Mobile.ViewModels;
using ReactiveUI.XamForms;
using System.Linq;
using ReactiveUI;

namespace Ignite.Abacus.Mobile
{	
	public partial class OptionsView : ReactiveContentPage<OptionsViewModel>
	{	
		public OptionsView ()
		{
			InitializeComponent ();

			// Setup the KeypadStyle Picker.
			KeypadPicker.ItemsSource = Enum.GetValues (typeof(KeypadStyle)).Cast<KeypadStyle> ().ToList();
			KeypadPicker.SourceItemLabelConverter = value => value.ToString();
			this.OneWayBind (ViewModel, x => x.CalculatorVM.KeypadStyle, x => x.KeypadPicker.SelectedItem);
			this.WhenAnyValue (x => x.KeypadPicker.SelectedItem).Subscribe (x => {
				var item = x;
				if (x != null && ViewModel.CalculatorVM.KeypadStyle != (KeypadStyle)x)
					ViewModel.CalculatorVM.SetKeypadStyleCommand.Execute(x);
			});

			// Setup the AdapterButtonsLocation Picker.
			ButtonLocationPicker.ItemsSource = Enum.GetValues (typeof(AdapterButtonsLocation)).Cast<AdapterButtonsLocation> ().ToList ();
			ButtonLocationPicker.SourceItemLabelConverter = value => value.ToString();
			this.OneWayBind (ViewModel, x => x.CalculatorVM.AdapterButtonsLocation, x => x.ButtonLocationPicker.SelectedItem);
			this.WhenAnyValue (x => x.ButtonLocationPicker.SelectedItem).Subscribe (x => {
				if (x != null && ViewModel.CalculatorVM.AdapterButtonsLocation != (AdapterButtonsLocation)x)
					ViewModel.CalculatorVM.SetAdapterButtonsLocationCommand.Execute(x);
			});

			// Setup the Calculator Adapter Picker.
			AdapterPicker.SourceItemLabelConverter = adapter => ((ICalculatorAdapter)adapter).Name;
			this.OneWayBind (ViewModel, x => x.CalculatorVM.AvailableAdapters, x => x.AdapterPicker.ItemsSource);
			this.OneWayBind (ViewModel, x => x.CalculatorVM.CalculatorAdapter, x => x.AdapterPicker.SelectedItem);
			this.WhenAnyValue (x => x.AdapterPicker.SelectedItem).Subscribe (x => {
				if (x != null && ViewModel.CalculatorVM.CalculatorAdapter != (ICalculatorAdapter)x)
					ViewModel.CalculatorVM.SetCalculatorAdapterCommand.Execute(x);
			});
		}
	}
}


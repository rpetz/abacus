﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Ignite.Abacus.Mobile
{
	public static class ExtensionMethods
	{
		/// <summary>
		/// Takes a collection of elements and breaks it into chunks of the specified length
		/// </summary>
		/// <typeparam name="T">The type of each element in the collection</typeparam>
		/// <param name="source">The source collection to split</param>
		/// <param name="maxBlockSize">The size of each chunk</param>
		/// <returns>A collection of chunks of elements</returns>
		public static IEnumerable<IEnumerable<T>> SplitIntoBlocks<T>(this IEnumerable<T> source, int maxBlockSize)
		{
			var modified = source.ToList();

			while (modified.Any()) {
				// This ensures that we don't try to take too large of a block.
				var blockSize = Math.Min(maxBlockSize, modified.Count);
				var selected = modified.Take(blockSize);
				yield return new List<T>(selected);
				modified.RemoveRange(0, blockSize);
			}
		}
	}
}
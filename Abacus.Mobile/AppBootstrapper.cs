﻿using Ignite.Abacus.Mobile.ViewModels;
using ReactiveUI;
using ReactiveUI.XamForms;
using Splat;
using Xamarin.Forms;

namespace Ignite.Abacus.Mobile
{
	public class AppBootstrapper : ReactiveObject, IScreen
	{
		public RoutingState Router { get; protected set; }

		public AppBootstrapper() 
		{
			Router = new RoutingState ();

			Locator.CurrentMutable.RegisterConstant (this, typeof(IScreen));

			Locator.CurrentMutable.Register (() => new CalculatorView (), typeof(IViewFor<CalculatorViewModel>));
			Locator.CurrentMutable.Register (() => new OptionsView (), typeof(IViewFor<OptionsViewModel>));
			Router.Navigate.Execute (new CalculatorViewModel());
		}

		public Page CreateMainPage()
		{
			return new RoutedViewHost ();
		}
	}
}
﻿using Ignite.Abacus.Mobile.Interfaces;
using Ignite.Abacus.Mobile.Models;
using Ignite.Abacus.Mobile.Objects;
using ReactiveUI;
using Splat;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xamarin;
using System.Collections.Generic;

namespace Ignite.Abacus.Mobile.ViewModels
{
	/// <summary>
	/// Provides a standard implementation of the <see cref="ICalculatorViewModel"/> interface.
	/// </summary>
	public class CalculatorViewModel : ReactiveObject, ICalculatorViewModel, IRoutableViewModel
	{
		public string UrlPathSegment {
			get { return "Calculator"; }
		}

		public IScreen HostScreen { get; protected set; }

#region Private Properties
		// Readonly Fields

		private readonly IButtonGeneratorService _buttonGeneratorService;

		// Logic Fields

		// This is used to switch between clearing just the display and clearing the current equation.
		private Boolean _displayDirty;
		private Boolean _lastEquationValueWasConstant;
		private Boolean _lastEquationValueWasOperator;

		// Backing Fields

		private string _bufferText;
		private string _equationText;
		private int _buttonColumnCount;
		private int _buttonRowCount;
		private KeypadStyle _keypadStyle;
		private AdapterButtonsLocation _adapterButtonsLocation;
		private ICalculatorAdapter _calculatorAdapter;
		private ReactiveList<ICalculatorButtonModel> _buttons;
		private ReactiveList<ICalculatorAdapter> _availableAdapters;

#endregion

		/// <summary>
		/// Creates a new instance of a CalculatorViewModel
		/// </summary>
		public CalculatorViewModel(IScreen hostScreen = null)
		{
			HostScreen = hostScreen ?? Locator.Current.GetService<IScreen> ();
			Locator.CurrentMutable.RegisterConstant (this, typeof(ICalculatorViewModel));
			AvailableAdapters = new ReactiveList<ICalculatorAdapter> (Locator.Current.GetServices<ICalculatorAdapter>());
			_buttonGeneratorService = Locator.Current.GetService<IButtonGeneratorService>();
			CalculatorAdapter = Locator.Current.GetServices<ICalculatorAdapter> ().FirstOrDefault(x => x.Name == Resources.LocalAdapterName);

			SetKeypadStyleCommand = ReactiveCommand.Create ();
			SetKeypadStyleCommand.Subscribe (x => { 
				if (x is MenuSelectionItem)
					KeypadStyle = (KeypadStyle)((MenuSelectionItem)x).Value;
				else if (x is KeypadStyle)
					KeypadStyle = (KeypadStyle)x;
			});

			SetAdapterButtonsLocationCommand = ReactiveCommand.Create ();
			SetAdapterButtonsLocationCommand.Subscribe(x => {  
				if (x is MenuSelectionItem)
					AdapterButtonsLocation = (AdapterButtonsLocation)((MenuSelectionItem)x).Value; 
				else if (x is AdapterButtonsLocation)
					AdapterButtonsLocation = (AdapterButtonsLocation)x;
			});

			SetCalculatorAdapterCommand = ReactiveCommand.Create ();
			SetCalculatorAdapterCommand.Subscribe(x => {  
				var adapter = (ICalculatorAdapter)x;
				CalculatorAdapter = adapter; 
			});

			// When any of the values change that have an effect on the currently displayed buttons...
			this.WhenAnyValue (x => x.CalculatorAdapter, x => x.AdapterButtonsLocation, x => x.KeypadStyle).Subscribe(_ => {
				// ...Clear the display and rebuild the buttons.
				ForceFullClear();
				RebuildButtons();
			});

			// When the buffer text changes...
			this.WhenAnyValue (x => x.BufferText).Subscribe(b => {
				// ...Set the display to dirty if the new text is not empty.
				_displayDirty = b != String.Empty;
			});

			// When the available buttons change...
			this.WhenAnyValue (x => x.Buttons).Subscribe(b => {
				// ...Update the row and column counts when the buttons change.
				if (_buttons == null) return;
				ButtonColumnCount = _buttons.Max(x => x.Column + x.ColumnSpan);
				ButtonRowCount = _buttons.Max(x => x.Row + x.RowSpan);
			});

			CalculatorButtonCommand = ReactiveCommand.CreateAsyncTask(async (x) =>
				{
					var button = (ICalculatorButtonModel)x;
					if (button.ButtonType == ButtonType.Clear) HandleClearCommand();
					else if (button.ButtonType == ButtonType.Equals) await HandleEqualsButton();
					else HandleButtonPress(button);
				});

			GoToOptionsCommand = ReactiveCommand.CreateAsyncObservable (_ => HostScreen.Router.Navigate.ExecuteAsync(Locator.Current.GetService<OptionsViewModel>()));
		}

#region Commands
		public IReactiveCommand CalculatorButtonCommand { get; set; }
		public ReactiveCommand<Object> GoToOptionsCommand { get; set; }
		public ReactiveCommand<Object> SetKeypadStyleCommand { get; set; }
		public ReactiveCommand<Object> SetAdapterButtonsLocationCommand { get; set; }
		public ReactiveCommand<Object> SetCalculatorAdapterCommand { get; set; }
#endregion

#region Properties
		public ICalculatorAdapter CalculatorAdapter
		{
			get { return _calculatorAdapter; }
			set { this.RaiseAndSetIfChanged (ref _calculatorAdapter, value); }
		}

		public AdapterButtonsLocation AdapterButtonsLocation
		{
			get { return _adapterButtonsLocation; }
			set { this.RaiseAndSetIfChanged (ref _adapterButtonsLocation, value); }
		}

		public KeypadStyle KeypadStyle
		{
			get { return _keypadStyle; }
			set { this.RaiseAndSetIfChanged (ref _keypadStyle, value); }
		}

		public int ButtonRowCount
		{
			get { return _buttonRowCount; }
			set { this.RaiseAndSetIfChanged (ref _buttonRowCount, value); }
		}

		public int ButtonColumnCount
		{
			get { return _buttonColumnCount; }
			set { this.RaiseAndSetIfChanged (ref _buttonColumnCount, value); }
		}

		public ReactiveList<ICalculatorAdapter> AvailableAdapters
		{
			get { return _availableAdapters; }
			set { this.RaiseAndSetIfChanged (ref _availableAdapters, value); }
		}

		public string EquationText
		{
			get { return _equationText; }
			set { this.RaiseAndSetIfChanged (ref _equationText, value); }
		}

		public string BufferText
		{
			get { return _bufferText; }
			set { this.RaiseAndSetIfChanged (ref _bufferText, value); }
		}

		public ReactiveList<ICalculatorButtonModel> Buttons
		{
			get { return _buttons; }
			set { this.RaiseAndSetIfChanged (ref _buttons, value); }
		}
#endregion

#region Private Logic
		private void RebuildButtons()
		{
			if (_calculatorAdapter != null)
				Buttons = new ReactiveList<ICalculatorButtonModel>(_buttonGeneratorService.GenerateButtonGrid (KeypadStyle, AdapterButtonsLocation, CalculatorAdapter.GetAvailableButtons ()));
		}

		private void ForceFullClear()
		{
			// To fully clear the display, remove the dirty flag.
			_displayDirty = false;
			HandleClearCommand();
		}

		private void HandleButtonPress(ICalculatorButtonModel button)
		{
			if (button.ActionType == ButtonActionType.Direct)
			{
				// Direct buttons append to the buffer.

				// If they press a direct input constant button, we need to clear the equation line
				// in the event that the last equation value was also a constant.
				if (_lastEquationValueWasConstant) EquationText = String.Empty;
				// Then toss the button's text onto the buffer.
				BufferText += button.Execute(String.Empty);
				_lastEquationValueWasOperator = false;
			}
			else if (button.ActionType == ButtonActionType.Operator)
			{
				// Operator buttons go directly on the equation line.
				_lastEquationValueWasConstant = false;
				// If the last equation was an operator, then we need to replace the operator.
				if (_lastEquationValueWasOperator)
					EquationText = EquationText.Substring(0, EquationText.Length - 1);
				EquationText += BufferText;
				BufferText = String.Empty;
				EquationText += button.Execute(String.Empty);
				_lastEquationValueWasOperator = true;
			}
			else if (button.ActionType == ButtonActionType.Expression)
			{
				// Expression buttons evaluate the buffer and move it to the equation line.

				// Expression buttons need to evaluate the buffer text before placing it's result on the equation line.
				// We can't evaluate the buffer if it's empty, so don't allow expressions when the buffer is empty.
				if (String.IsNullOrWhiteSpace (BufferText))
					return;
				_lastEquationValueWasConstant = false;
				EquationText += button.Execute(BufferText);
				BufferText = String.Empty;
				_lastEquationValueWasOperator = false;
			}
			else if (button.ActionType == ButtonActionType.Inline)
			{
				// Inline buttons evaluate the buffer and replace it with the result.

				// If they press an inline constant button, we need to clear the equation line
				// in the event that the last equation value was also a constant.
				if (_lastEquationValueWasConstant) EquationText = String.Empty;
				BufferText = button.Execute(BufferText);
				_lastEquationValueWasOperator = false;
			}
		}

		private async Task HandleEqualsButton()
		{
			EquationText += BufferText;
			// Get the result of the equation.
			var result = await CalculatorAdapter.PerformCalculation(EquationText);
			// Clear the display completely.
			ForceFullClear();
			// Set the equation to the result of the calculation.
			EquationText = result;
			_lastEquationValueWasConstant = true;
		}

		private void HandleClearCommand()
		{
			if (_displayDirty)
				// On first click, just clear the display.
				BufferText = String.Empty;
			else
			{
				// On subsequent click clear the equation, display text, and button press sequence.
				EquationText = String.Empty;
				BufferText = String.Empty;
			}
		}
#endregion
	}
}
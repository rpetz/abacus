﻿using Ignite.Abacus.Mobile.Interfaces;
using ReactiveUI;
using Splat;

namespace Ignite.Abacus.Mobile.ViewModels
{
	public class OptionsViewModel : ReactiveObject, IRoutableViewModel
	{
		ICalculatorViewModel _calculatorVM;

		public string UrlPathSegment {
			get { return "Options"; }
		}

		public IScreen HostScreen { get; protected set; }

		public ICalculatorViewModel CalculatorVM {
			get { return _calculatorVM; }
			set { this.RaiseAndSetIfChanged (ref _calculatorVM, value); }
		}

		public OptionsViewModel (IScreen hostScreen = null)
		{
			HostScreen = hostScreen ?? Locator.Current.GetService<IScreen> ();
			CalculatorVM = Locator.Current.GetService<ICalculatorViewModel> (); 
		}
	}
}


﻿using System;
using System.Linq;
using System.Windows.Input;
using Ignite.Abacus.Mobile.Interfaces;
using Ignite.Abacus.Mobile.Objects;
using Splat;

namespace Ignite.Abacus.WPF
{
	public partial class MainWindow
	{
		private ICalculatorViewModel _viewModel;

		public MainWindow()
		{
			InitializeComponent();

			ViewModel = Locator.Current.GetService<ICalculatorViewModel>();

			// Setup event handlers.
			PreviewTextInput += OnPreviewTextInput;
			KeyUp += OnKeyUp;
		}

		public ICalculatorViewModel ViewModel
		{
			get { return _viewModel; }
			set
			{
				_viewModel = value;
				DataContext = _viewModel;
			}
		}

		private void OnPreviewTextInput(object sender, TextCompositionEventArgs args)
		{
			// We handle the PreviewTextInput event because it provides a way to check if they press modified keys,
			// such as the parenthesis key (usually shift+9).

			// When text is being entered, check if we have a button that matches it and execute it.
			var text = args.Text;
			var button = ViewModel.Buttons.FirstOrDefault(x => String.Equals(x.TitleText, text, StringComparison.CurrentCultureIgnoreCase));

			if (button != null)
				ViewModel.CalculatorButtonCommand.Execute(button);
		}

		private void OnKeyUp(object sender, KeyEventArgs args)
		{
			// Get the clear and equals buttons from the view model.
			var clearButton = ViewModel.Buttons.FirstOrDefault(x => x.ButtonType == ButtonType.Clear);
			var equalsButton = ViewModel.Buttons.FirstOrDefault(x => x.ButtonType == ButtonType.Equals);

			// Check if a modifier key is currently being pressed by ORing the possible options together and then
			// anding the result with the Modifiers enum.  In the event they are using a modifier key, don't do anything.
			if ((Keyboard.Modifiers & (ModifierKeys.Shift | ModifierKeys.Control | ModifierKeys.Alt)) != 0)
				return;

			// If they are pressing the enter or escape key, call the equals or clear button respectively.
			if (args.Key == Key.Enter)
				ViewModel.CalculatorButtonCommand.Execute(equalsButton);
			else if (args.Key == Key.Escape)
				ViewModel.CalculatorButtonCommand.Execute(clearButton);
		}

		~MainWindow()
		{
			// Clean up the event handlers on deconstruction.
			PreviewTextInput -= OnPreviewTextInput;
			KeyUp -= OnKeyUp;
		}
	}
}

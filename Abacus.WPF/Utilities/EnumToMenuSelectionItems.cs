﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.Windows.Markup;
using Ignite.Abacus.Mobile.Models;

namespace Ignite.Abacus.WPF.Utilities
{
	/// <summary>
	/// Provides a markup extension to convert an enum into a collection of MenuSelectionItems.
	/// </summary>
	public class EnumToMenuSelectionItems : MarkupExtension
	{
		private readonly Type _type;

		/// <summary>
		/// Constructs a new instance of EnumToMenuSelectionItems.
		/// </summary>
		/// <param name="type">The enum type to use</param>
		public EnumToMenuSelectionItems(Type type) { _type = type; }

		public override object ProvideValue(IServiceProvider serviceProvider)
		{
			// Get the enum values.
			var enumValues = Enum.GetValues(_type).Cast<object>().ToList();
			return enumValues.Select(e => {
				// Get the type of the enum.
				var targetType = e.GetType();
				// Get the enum's name.
				var name = Enum.GetName(targetType, e);
				// Check if the enum has an EnumMemberAttribute for sourcing it's value.
				var attr = ((EnumMemberAttribute[]) targetType.GetField(name).GetCustomAttributes(typeof (EnumMemberAttribute), true)).FirstOrDefault();
				// Build a response object utilizing the aquired values.
				return new MenuSelectionItem((int)e, attr == null ? name : attr.Value, enumValues.IndexOf(e) == 0);
			}); 
		}
	}
}
﻿using System.Windows;
using Ignite.Abacus.Mobile.Interfaces;
using Ignite.Abacus.WPF.Services;
using Splat;

namespace Ignite.Abacus.WPF
{
	public partial class App
	{
		protected override void OnStartup(StartupEventArgs e)
		{
			base.OnStartup(e);

			Mobile.App.BindDependencies();
			Locator.CurrentMutable.Register(() => new ConsoleService(), typeof(IConsoleService));

			MainWindow = new MainWindow();
			MainWindow.Show();
		}
	}
}

﻿using System;
using System.Windows.Data;
using System.Windows.Media;

namespace Ignite.Abacus.WPF.ValueConverters
{
	/// <summary>
	/// Provides a converter to convert between Color and SolidColorBrush.
	/// </summary>
	public class ColorToSolidColorBrushConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			if (null == value)
			{
				return null;
			}
			if (value is Color)
				return new SolidColorBrush((Color)value);

			var type = value.GetType();
			throw new InvalidOperationException("Unsupported type [" + type.Name + "]");
		}

		public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
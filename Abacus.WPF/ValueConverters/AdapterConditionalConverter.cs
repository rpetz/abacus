﻿using System;
using System.Globalization;
using System.Windows.Data;
using Ignite.Abacus.Mobile.Interfaces;

namespace Ignite.Abacus.WPF.ValueConverters
{
	/// <summary>
	/// Provides a conditional value converter for determining if a CalculatorAdapter matches another CalculatorAdapter.
	/// </summary>
	public class AdapterConditionalConverter : IMultiValueConverter
	{
		public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
		{
			var sourceValue = (ICalculatorAdapter)values[0];
			var targetValue = (ICalculatorAdapter)values[1];

			return sourceValue.Name == targetValue.Name;
		}

		public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture) { throw new NotImplementedException(); }
	}
}
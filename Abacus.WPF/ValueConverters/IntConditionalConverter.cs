﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Ignite.Abacus.WPF.ValueConverters
{
	/// <summary>
	/// Provides a conditional value converter for determining if an int matches another int.
	/// </summary>
	public class IntConditionalConverter : IMultiValueConverter
	{
		public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
		{
			var sourceValue = (int)values[0];
			var targetValue = (int)values[1];

			return sourceValue == targetValue;
		}

		public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture) { throw new NotImplementedException(); }
	}
}
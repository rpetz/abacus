#Abacus
	Robert Petz
	December 2014

A cross platform calculator with pluggable calculator engines.  


###Overview

The solution is broken up into three projects:

**Abacus.Mobile** - This project contains the core implementation of Abacus.

**Abacus.iOS** - This project contains the iOS wrapper around Abacus.Mobile.

**Abacus.WPF** - This project contains the WPF wrapper around Abacus.Mobile.

**Abacus.Test** - This project contains all unit tests.

####Calculator Adapters

Out of the box, three calculation engines are currently supported:

#####[Calcatraz](https://www.calcatraz.com/calculator/) - `Web`
#####[Wolfram|Alpha](http://products.wolframalpha.com/api/) - `Web`
#####[MathosParser](https://mathosparser.codeplex.com/) - `Local`


####Supported Platforms

As of right now `WPF` and `iOS` are the only supported platforms - However, porting to Android would only require implementing platform specific renderers for the controls under the  Ignite.Abacus.iOS.Controls namespace.

####Key Features

- ######Service Location

- ######Pluggable Calculator Adapters

- ######MVVM

- ######Async/Await

- ######Unit Testing

- ######Xamarin UITest

- ######Observables (Through ReactiveUI/Reactive Extensions)
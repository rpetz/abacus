﻿using System;
using System.Threading.Tasks;
using Ignite.Abacus.Mobile;
using Ignite.Abacus.Mobile.Adapters;
using Ignite.Abacus.Mobile.Interfaces;
using Moq;
using NUnit.Framework;
using Splat;

namespace Abacus.Test
{
	[TestFixture]
	public class WolphramAlphaAdapterTests
	{
		[Test]
		public void ShouldHaveAName()
		{
			var mockWebSvc = new Mock<IWebAccessService>();
			Locator.CurrentMutable.RegisterConstant(mockWebSvc.Object, typeof(IWebAccessService));
			var svc = new WolframAlphaAdapter();

			Assert.IsFalse(String.IsNullOrWhiteSpace(svc.Name));
		}

		[Test]
		public void ShouldInvokeWebService()
		{
			var mockWebSvc = new Mock<IWebAccessService>();
			Locator.CurrentMutable.RegisterConstant(mockWebSvc.Object, typeof(IWebAccessService));
			var svc = new WolframAlphaAdapter();

			svc.PerformCalculation(String.Empty);
			mockWebSvc.Verify(x => x.CreateWebRequest(It.IsAny<String>()), Times.Once());
		}

		[Test]
		public void ShouldGracefullyHandleServerException()
		{
			var mockWebSvc = new Mock<IWebAccessService>();
			Locator.CurrentMutable.RegisterConstant(mockWebSvc.Object, typeof(IWebAccessService));
			var svc = new WolframAlphaAdapter();

			mockWebSvc.Setup(x => x.CreateWebRequest(It.IsAny<String>())).Throws(new Exception());

			var gotException = false;
			try
			{
				var result = svc.PerformCalculation(String.Empty).Result;
				Assert.AreEqual(result, Resources.ErrorParsingEquation);
			}
			catch
			{
				gotException = true;
			}

			Assert.IsFalse(gotException);
		}

		[Test]
		public void ShouldHandleServiceFailureGracefully()
		{
			var mockWebSvc = new Mock<IWebAccessService>();
			Locator.CurrentMutable.RegisterConstant(mockWebSvc.Object, typeof(IWebAccessService));
			var svc = new WolframAlphaAdapter();

			mockWebSvc.Setup(x => x.CreateWebRequest(It.IsAny<String>())).Returns(Task.FromResult(@"<?xml version='1.0' encoding='UTF-8'?><queryresult success='false' error='false'></queryresult>"));

			var result = svc.PerformCalculation(String.Empty).Result;

			Assert.AreEqual(result, Resources.ErrorParsingEquation);
		}

		[Test]
		public void ShouldHandleServiceErrorGracefully()
		{
			var mockWebSvc = new Mock<IWebAccessService>();
			Locator.CurrentMutable.RegisterConstant(mockWebSvc.Object, typeof(IWebAccessService));
			var svc = new WolframAlphaAdapter();

			mockWebSvc.Setup(x => x.CreateWebRequest(It.IsAny<String>())).Returns(Task.FromResult(@"<?xml version='1.0' encoding='UTF-8'?><queryresult success='true' error='true'></queryresult>"));

			var result = svc.PerformCalculation(String.Empty).Result;

			Assert.AreEqual(result, Resources.ErrorParsingEquation);
		}
	}
}
﻿using System;
using System.Linq;
using System.IO;
using System.Reflection;
using NUnit.Framework;
using Xamarin.UITest;
using Xamarin.UITest.Queries;

namespace Abacus.UITest
{
	[TestFixture]
	public class MobileUITests
	{
		#region setup
		private IApp _app;
		public string PathToIPA { get; set; }

		[SetUp]
		public void SetUp()
		{
			_app = ConfigureApp.iOS.AppBundle(PathToIPA).StartApp();
		}

		[TestFixtureSetUp]
		public void TestFixtureSetup()
		{
			var currentFile = new Uri(Assembly.GetExecutingAssembly().CodeBase).LocalPath;
			var fi = new FileInfo(currentFile);
			var dir = fi.Directory.Parent.Parent.Parent.FullName;
			PathToIPA = Path.Combine(dir, "Abacus.iOS", "bin", "iPhoneSimulator", "Debug", "AbacusiOS.app");
		}
		#endregion

		Func<AppQuery, AppQuery> optionsBtnQuery = c => c.Text("Options").Class("UIButtonLabel");
		Func<AppQuery, AppQuery> equationLabel = c => c.Marked("EquationLbl");
		Func<AppQuery, AppQuery> bufferLabel = c => c.Marked("BufferLbl");
		Func<AppQuery, AppQuery> selectedAdapterLabel = c => c.Marked("SelectedAdapterLbl");
		Func<AppQuery, AppQuery> calculatorButtonGrid = c => c.Marked("CalcBtnGrid");
		Func<AppQuery, AppQuery> equalsButton = c => c.Marked("CalcBtn").Text("=");
		Func<AppQuery, AppQuery> clearButton = c => c.Marked("CalcBtn").Text("Clear");
		Func<AppQuery, AppQuery> calcButtons = c => c.Marked("CalcBtn");
		Func<AppQuery, AppQuery> adapterPicker = c => c.Marked("AdapterPicker");
		Func<AppQuery, AppQuery> keypadPicker = c => c.Marked("KeypadStylePicker");
		Func<AppQuery, AppQuery> locationPicker = c => c.Marked("AdapterButtonsLocationPicker");

		[Test]
		public void ShouldHaveOptionsButtonOnInitialCalculatorView()
		{
			Assert.IsTrue (_app.Query(optionsBtnQuery).Count() == 1, "The options button did not appear on the initial view");
		}

		[Test]
		public void ShouldClearOptionsButtonFromTitleBarOnNavigationToOptions()
		{
			_app.Tap (optionsBtnQuery);

			_app.WaitForNoElement (optionsBtnQuery, timeout: TimeSpan.FromSeconds(5));

			Assert.IsFalse (_app.Query(optionsBtnQuery).Any (), "The options button did not go away when navigating to the options view");
		}

		[Test]
		public void ShouldAddOptionsButtonToTitleBarOnNavigationToCalculator()
		{
			// Move to the OptionsView first.
			_app.Tap (optionsBtnQuery);

			_app.WaitForNoElement (optionsBtnQuery, timeout: TimeSpan.FromSeconds(5));
			Assert.IsFalse (_app.Query(optionsBtnQuery).Any (), "The options button did not go away when navigating to the options view");

			_app.Back ();

			_app.WaitForElement (optionsBtnQuery, timeout: TimeSpan.FromSeconds(5));
			Assert.IsTrue (_app.Query(optionsBtnQuery).Count() == 1, "The options button did not go away when navigating to the options view");
		}
	}
}

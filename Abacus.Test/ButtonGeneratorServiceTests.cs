﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ignite.Abacus.Mobile.Interfaces;
using Ignite.Abacus.Mobile.Models;
using Ignite.Abacus.Mobile.Objects;
using Ignite.Abacus.Mobile.Services;
using NUnit.Framework;

namespace Abacus.Test
{
	[TestFixture]
	public class ButtonGeneratorServiceTests
	{
		[Test]
		public void ShouldCreateStandardButtons()
		{
			var svc = new ButtonGeneratorService();
			var buttons = svc.GenerateButtonGrid(KeypadStyle.Calculator, AdapterButtonsLocation.Bottom, new List<ICalculatorButtonModel>());

			var standardButtons = new List<String> {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", ".", "clear", "=", "(", ")", "/", "*", "+", "-"};

			Assert.IsTrue(buttons.Count(x => standardButtons.Contains(x.TitleText.ToLower())) == standardButtons.Count);
		}

		[Test]
		public void ShouldPlaceStandardButtonsInCorrectGridLocations()
		{
			var svc = new ButtonGeneratorService();
			var buttons = svc.GenerateButtonGrid(KeypadStyle.Calculator, AdapterButtonsLocation.Bottom, new List<ICalculatorButtonModel>()).ToList();

			var numBtn0 = buttons.FirstOrDefault(x => x.TitleText == "0");
			var numBtn1 = buttons.FirstOrDefault(x => x.TitleText == "1");
			var numBtn2 = buttons.FirstOrDefault(x => x.TitleText == "2");
			var numBtn3 = buttons.FirstOrDefault(x => x.TitleText == "3");
			var numBtn4 = buttons.FirstOrDefault(x => x.TitleText == "4");
			var numBtn5 = buttons.FirstOrDefault(x => x.TitleText == "5");
			var numBtn6 = buttons.FirstOrDefault(x => x.TitleText == "6");
			var numBtn7 = buttons.FirstOrDefault(x => x.TitleText == "7");
			var numBtn8 = buttons.FirstOrDefault(x => x.TitleText == "8");
			var numBtn9 = buttons.FirstOrDefault(x => x.TitleText == "9");
			var dotBtn			= buttons.FirstOrDefault(x => x.TitleText == ".");
			var clearBtn		= buttons.FirstOrDefault(x => x.TitleText.ToLower() == "clear");
			var equalsBtn		= buttons.FirstOrDefault(x => x.TitleText == "=");
			var openParenBtn	= buttons.FirstOrDefault(x => x.TitleText == "(");
			var closeParenBtn	= buttons.FirstOrDefault(x => x.TitleText == ")");
			var divideBtn		= buttons.FirstOrDefault(x => x.TitleText == "/");
			var multiplyBtn		= buttons.FirstOrDefault(x => x.TitleText == "*");
			var addBtn			= buttons.FirstOrDefault(x => x.TitleText == "+");
			var subtractBtn		= buttons.FirstOrDefault(x => x.TitleText == "-");

			Assert.IsTrue(numBtn0.Row == 4		 && numBtn0.Column == 0		  && numBtn0.ColumnSpan == 1	   && numBtn0.RowSpan == 1);
			Assert.IsTrue(numBtn1.Row == 3		 && numBtn1.Column == 0		  && numBtn1.ColumnSpan == 1	   && numBtn1.RowSpan == 1);
			Assert.IsTrue(numBtn2.Row == 3		 && numBtn2.Column == 1		  && numBtn2.ColumnSpan == 1	   && numBtn2.RowSpan == 1);
			Assert.IsTrue(numBtn3.Row == 3		 && numBtn3.Column == 2		  && numBtn3.ColumnSpan == 1	   && numBtn3.RowSpan == 1);
			Assert.IsTrue(numBtn4.Row == 2		 && numBtn4.Column == 0		  && numBtn4.ColumnSpan == 1	   && numBtn4.RowSpan == 1);
			Assert.IsTrue(numBtn5.Row == 2		 && numBtn5.Column == 1		  && numBtn5.ColumnSpan == 1	   && numBtn5.RowSpan == 1);
			Assert.IsTrue(numBtn6.Row == 2		 && numBtn6.Column == 2		  && numBtn6.ColumnSpan == 1	   && numBtn6.RowSpan == 1);
			Assert.IsTrue(numBtn7.Row == 1		 && numBtn7.Column == 0		  && numBtn7.ColumnSpan == 1	   && numBtn7.RowSpan == 1);
			Assert.IsTrue(numBtn8.Row == 1		 && numBtn8.Column == 1		  && numBtn8.ColumnSpan == 1	   && numBtn8.RowSpan == 1);
			Assert.IsTrue(numBtn9.Row == 1		 && numBtn9.Column == 2		  && numBtn9.ColumnSpan == 1	   && numBtn9.RowSpan == 1);
			Assert.IsTrue(dotBtn.Row == 4		 && dotBtn.Column == 1		  && dotBtn.ColumnSpan == 1		   && dotBtn.RowSpan == 1);
			Assert.IsTrue(clearBtn.Row == 0		 && clearBtn.Column == 0	  && clearBtn.ColumnSpan == 1	   && clearBtn.RowSpan == 1);
			Assert.IsTrue(equalsBtn.Row == 4	 && equalsBtn.Column == 2	  && equalsBtn.ColumnSpan == 2	   && equalsBtn.RowSpan == 1);
			Assert.IsTrue(openParenBtn.Row == 0	 && openParenBtn.Column == 1  && openParenBtn.ColumnSpan == 1  && openParenBtn.RowSpan == 1);
			Assert.IsTrue(closeParenBtn.Row == 0 && closeParenBtn.Column == 2 && closeParenBtn.ColumnSpan == 1 && closeParenBtn.RowSpan == 1);
			Assert.IsTrue(divideBtn.Row == 0	 && divideBtn.Column == 3	  && divideBtn.ColumnSpan == 1	   && divideBtn.RowSpan == 1);
			Assert.IsTrue(multiplyBtn.Row == 1	 && multiplyBtn.Column == 3	  && multiplyBtn.ColumnSpan == 1   && multiplyBtn.RowSpan == 1);
			Assert.IsTrue(addBtn.Row == 3		 && addBtn.Column == 3		  && addBtn.ColumnSpan == 1		   && addBtn.RowSpan == 1);
			Assert.IsTrue(subtractBtn.Row == 2	 && subtractBtn.Column == 3	  && subtractBtn.ColumnSpan == 1   && subtractBtn.RowSpan == 1);
		}					 

		[Test]
		public void ShouldNotBreakOnNullAdapterButtons()
		{
			var svc = new ButtonGeneratorService();
			var gotException = false;
			try
			{ svc.GenerateButtonGrid(KeypadStyle.Calculator, AdapterButtonsLocation.Bottom, null); }
			catch 
			{ gotException = true; }

			Assert.IsFalse(gotException);
		}

		[Test]
		public void ShouldCreateAdapterButtons()
		{
			var svc = new ButtonGeneratorService();
			var buttons = svc.GenerateButtonGrid(KeypadStyle.Calculator, AdapterButtonsLocation.Bottom, new List<ICalculatorButtonModel> {
				new CalculatorButtonModel("test") { Row = 0, Column = 0 }
			});

			Assert.IsNotNull(buttons.FirstOrDefault(x => x.TitleText == "test"));
		}

		[Test]
		public void ShouldPlaceAdapterButtonsOnRight()
		{
			var svc = new ButtonGeneratorService();
			var buttons = svc.GenerateButtonGrid(KeypadStyle.Calculator, AdapterButtonsLocation.Right, new List<ICalculatorButtonModel> {
				new CalculatorButtonModel("test") { Row = 0, Column = 0 }
			});

			var standardButtons = buttons.Where(x => x.TitleText != "test").ToList();
			var testButton = buttons.FirstOrDefault(x => x.TitleText == "test");

			Assert.IsTrue(standardButtons.Min(x => x.Column) == 0);
			Assert.IsTrue(standardButtons.Min(x => x.Row) == 0);
			// We don't add '1' to this because columnspan has a minimum value of '1', which means it takes up one column.
			Assert.IsTrue(testButton.Column == standardButtons.Max(y => y.Column + y.ColumnSpan));
			Assert.IsTrue(testButton.Row == 0);
		}

		[Test]
		public void ShouldPlaceAdapterButtonsOnLeft()
		{
			var svc = new ButtonGeneratorService();
			var buttons = svc.GenerateButtonGrid(KeypadStyle.Calculator, AdapterButtonsLocation.Left, new List<ICalculatorButtonModel> {
				new CalculatorButtonModel("test") { Row = 0, Column = 0 }
			});

			var standardButtons = buttons.Where(x => x.TitleText != "test").ToList();
			var testButton = buttons.FirstOrDefault(x => x.TitleText == "test");

			Assert.IsTrue(standardButtons.Min(x => x.Column) == 1);
			Assert.IsTrue(standardButtons.Min(x => x.Row) == 0);
			Assert.IsTrue(testButton.Column == 0);
			Assert.IsTrue(testButton.Row == 0);

		}

		[Test]
		public void ShouldPlaceAdapterButtonsOnBottom()
		{
			var svc = new ButtonGeneratorService();
			var buttons = svc.GenerateButtonGrid(KeypadStyle.Calculator, AdapterButtonsLocation.Bottom, new List<ICalculatorButtonModel> {
				new CalculatorButtonModel("test") { Row = 0, Column = 0 }
			});

			var standardButtons = buttons.Where(x => x.TitleText != "test").ToList();
			var testButton = buttons.FirstOrDefault(x => x.TitleText == "test");

			Assert.IsTrue(standardButtons.Min(x => x.Column) == 0);
			Assert.IsTrue(standardButtons.Min(x => x.Row) == 0);
			Assert.IsTrue(testButton.Column == 0);
			// We don't add '1' to this because rowspan has a minimum value of '1', which means it takes up one row.
			Assert.IsTrue(testButton.Row == standardButtons.Max(y => y.Row + y.RowSpan));
		}

		[Test]
		public void ShouldCreateCalculatorKeypad()
		{
			var svc = new ButtonGeneratorService();
			var buttons = svc.GenerateButtonGrid(KeypadStyle.Calculator, AdapterButtonsLocation.Bottom, new List<ICalculatorButtonModel>()).ToList();

			var row1 = buttons.Where(x => new[] { "7", "8", "9" }.Contains(x.TitleText)).ToList();
			var row2 = buttons.Where(x => new[] { "4", "5", "6" }.Contains(x.TitleText)).ToList();
			var row3 = buttons.Where(x => new[] { "1", "2", "3" }.Contains(x.TitleText)).ToList();

			Assert.IsTrue(row1.Count == 3);
			Assert.IsTrue(row2.Count == 3);
			Assert.IsTrue(row3.Count == 3);
			// This is offset by 1 row because the first row non numeric buttons.
			Assert.IsTrue(row1.All(x => x.Row == 1));
			Assert.IsTrue(row2.All(x => x.Row == 2));
			Assert.IsTrue(row3.All(x => x.Row == 3));
		}

		[Test]
		public void ShouldCreatePhoneKeypad()
		{
			var svc = new ButtonGeneratorService();
			var buttons = svc.GenerateButtonGrid(KeypadStyle.Phone, AdapterButtonsLocation.Bottom, new List<ICalculatorButtonModel>()).ToList();

			var row1 = buttons.Where(x => new[] { "1", "2", "3" }.Contains(x.TitleText)).ToList();
			var row2 = buttons.Where(x => new[] { "4", "5", "6" }.Contains(x.TitleText)).ToList();
			var row3 = buttons.Where(x => new[] { "7", "8", "9" }.Contains(x.TitleText)).ToList();

			Assert.IsTrue(row1.Count == 3);
			Assert.IsTrue(row2.Count == 3);
			Assert.IsTrue(row3.Count == 3);
			// This is offset by 1 row because the first row non numeric buttons.
			Assert.IsTrue(row1.All(x => x.Row == 1));
			Assert.IsTrue(row2.All(x => x.Row == 2));
			Assert.IsTrue(row3.All(x => x.Row == 3));
		}
	}
}

﻿using System;
using Ignite.Abacus.Mobile.Adapters;
using NUnit.Framework;

namespace Abacus.Test
{
	[TestFixture]
	public class LocalAdapterTests
	{
		[Test]
		public void ShouldHaveAName()
		{
			var svc = new LocalAdapter();

			Assert.IsFalse(String.IsNullOrWhiteSpace(svc.Name));
		}

		// The local MathosParser adapter is a one liner to parse.  Unfortunately MathosParser uses
		// a static class API, which makes it practically impossible to mock.  While it could be argued
		// that a third party parsing system does not need testing here, it is still possible to test 
		// if you utilize a fakes assembly and a shim context.  However, Microsoft UnitTest libraries
		// don't appear to be supported by Xamarin Studio at the moment so there isn't much that can be
		// tested.


		// This is how a ShimsContext from a generated fakes assembly would be utilized.
//		[Test]
//		public void ShouldGracefullyHandleParseException()
//		{
//			using (ShimsContext.Create()) {
//				ShimMathParser.AllInstances.ParseString = (x,y) => { throw new Exception(); };
//
//				var svc = new LocalAdapter();
//
//				var gotException = false;
//				try
//				{
//					var result = svc.PerformCalculation(String.Empty).Result;
//					Assert.AreEqual(result, Resources.ErrorParsingEquation);
//				}
//				catch
//				{
//					gotException = true;
//				}
//
//				Assert.IsFalse(gotException);
//			}
//		}

		[Test]
		public void ShouldParseExpression()
		{
			var svc = new LocalAdapter();

			var result = svc.PerformCalculation("2*2").Result;
			Assert.AreEqual(result, "4");
		}
	}
}
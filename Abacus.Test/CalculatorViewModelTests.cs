﻿using System;
using System.Linq;
using Ignite.Abacus.Mobile.Adapters;
using Ignite.Abacus.Mobile.Interfaces;
using Ignite.Abacus.Mobile.Objects;
using Ignite.Abacus.Mobile.Services;
using Ignite.Abacus.Mobile.ViewModels;
using NUnit.Framework;
using Splat;

namespace Abacus.Test
{
	[TestFixture]
	public class CalculatorViewModelTests
	{
		[SetUp]
		public void SetUp()
		{
			Locator.CurrentMutable.RegisterConstant (new LocalAdapter(), typeof(ICalculatorAdapter));
			Locator.CurrentMutable.RegisterConstant (new ButtonGeneratorService(), typeof(IButtonGeneratorService));
		}

		[Test]
		public void ShouldOnlyClearBufferWithFirstClearPress()
		{
			var vm = new  CalculatorViewModel();

			var clearButton = vm.Buttons.FirstOrDefault(x => x.TitleText.ToLower() == "clear" );
			var oneBtn = vm.Buttons.FirstOrDefault(x => x.TitleText.ToLower() == "1");
			var addBtn = vm.Buttons.FirstOrDefault(x => x.TitleText.ToLower() == "+");

			vm.CalculatorButtonCommand.Execute(oneBtn);
			vm.CalculatorButtonCommand.Execute(addBtn);
			vm.CalculatorButtonCommand.Execute(oneBtn);
			vm.CalculatorButtonCommand.Execute(clearButton);

			Assert.IsTrue(vm.EquationText == "1+");
			Assert.IsTrue(String.IsNullOrEmpty(vm.BufferText));
		}

		[Test]
		public void ShouldClearEquationWithSecondClearPress()
		{
			var vm = new  CalculatorViewModel();

			var clearButton = vm.Buttons.FirstOrDefault(x => x.TitleText.ToLower() == "clear");
			var oneBtn = vm.Buttons.FirstOrDefault(x => x.TitleText.ToLower() == "1");
			var addBtn = vm.Buttons.FirstOrDefault(x => x.TitleText.ToLower() == "+");

			vm.CalculatorButtonCommand.Execute(oneBtn);
			vm.CalculatorButtonCommand.Execute(addBtn);
			vm.CalculatorButtonCommand.Execute(oneBtn);
			vm.CalculatorButtonCommand.Execute(clearButton);
			vm.CalculatorButtonCommand.Execute(clearButton);

			Assert.IsTrue(String.IsNullOrEmpty(vm.BufferText));
			Assert.IsTrue(String.IsNullOrEmpty(vm.EquationText));
		}

		[Test]
		public void ShouldRebuildButtonsWhenAdapterChanges()
		{
			var vm = new  CalculatorViewModel();

			var btnHash = vm.Buttons.GetHashCode();
			vm.CalculatorAdapter = new CalcatrazAdapter();
			var newHash = vm.Buttons.GetHashCode();

			Assert.IsTrue(btnHash != newHash);
		}

		[Test]
		public void ShouldRebuildButtonsWhenKeypadStyleChanges()
		{
			var vm = new  CalculatorViewModel();

			var btnHash = vm.Buttons.GetHashCode();
			vm.KeypadStyle = KeypadStyle.Phone;
			var newHash = vm.Buttons.GetHashCode();

			Assert.IsTrue(btnHash != newHash);
		}

		[Test]
		public void ShouldRebuildButtonsWhenAdapterButtonsLocationChanges()
		{
			var vm = new  CalculatorViewModel();

			var btnHash = vm.Buttons.GetHashCode();
			vm.AdapterButtonsLocation = AdapterButtonsLocation.Left;
			var newHash = vm.Buttons.GetHashCode();

			Assert.IsTrue(btnHash != newHash);
		}

		[Test]
		public void ShouldClearBufferAndEquationWhenAdapterChanges()
		{
			var vm = new  CalculatorViewModel();

			var oneBtn = vm.Buttons.FirstOrDefault(x => x.TitleText.ToLower() == "1");
			var addBtn = vm.Buttons.FirstOrDefault(x => x.TitleText.ToLower() == "+");
			vm.CalculatorButtonCommand.Execute(oneBtn);
			vm.CalculatorButtonCommand.Execute(addBtn);
			vm.CalculatorButtonCommand.Execute(oneBtn);

			var oldBuffer = vm.BufferText;
			var oldEquation = vm.EquationText;
			vm.AdapterButtonsLocation = AdapterButtonsLocation.Left;

			Assert.IsTrue(oldBuffer != vm.BufferText);
			Assert.IsTrue(oldEquation != vm.EquationText);
		}

		[Test]
		public void ShouldClearBufferAndEquationWhenKeypadStyleChanges()
		{
			var vm = new  CalculatorViewModel();

			var oneBtn = vm.Buttons.FirstOrDefault(x => x.TitleText.ToLower() == "1");
			var addBtn = vm.Buttons.FirstOrDefault(x => x.TitleText.ToLower() == "+");
			vm.CalculatorButtonCommand.Execute(oneBtn);
			vm.CalculatorButtonCommand.Execute(addBtn);
			vm.CalculatorButtonCommand.Execute(oneBtn);

			var oldBuffer = vm.BufferText;
			var oldEquation = vm.EquationText;
			vm.KeypadStyle = KeypadStyle.Phone;

			Assert.IsTrue(oldBuffer != vm.BufferText);
			Assert.IsTrue(oldEquation != vm.EquationText);
		}

		[Test]
		public void ShouldClearBufferAndEquationWhenAdapterButtonsLocationChanges()
		{
			var vm = new  CalculatorViewModel();

			var oneBtn = vm.Buttons.FirstOrDefault(x => x.TitleText.ToLower() == "1");
			var addBtn = vm.Buttons.FirstOrDefault(x => x.TitleText.ToLower() == "+");
			vm.CalculatorButtonCommand.Execute(oneBtn);
			vm.CalculatorButtonCommand.Execute(addBtn);
			vm.CalculatorButtonCommand.Execute(oneBtn);

			var oldBuffer = vm.BufferText;
			var oldEquation = vm.EquationText;
			vm.AdapterButtonsLocation = AdapterButtonsLocation.Left;

			Assert.IsTrue(oldBuffer != vm.BufferText);
			Assert.IsTrue(oldEquation != vm.EquationText);
		}

		[Test]
		public void OperatorPressShouldGoDirectlyOnEquationLine()
		{
			var vm = new  CalculatorViewModel();

			var addBtn = vm.Buttons.FirstOrDefault(x => x.TitleText.ToLower() == "+");
			vm.CalculatorButtonCommand.Execute(addBtn);
			Assert.IsTrue(vm.EquationText == "+");
			Assert.IsTrue(String.IsNullOrEmpty(vm.BufferText));
		}

		[Test]
		public void OperatorPressShouldReplaceOperatorWhenLastButtonWasOperator()
		{
			var vm = new  CalculatorViewModel();

			var addBtn = vm.Buttons.FirstOrDefault(x => x.TitleText.ToLower() == "+");
			var minusBtn = vm.Buttons.FirstOrDefault(x => x.TitleText.ToLower() == "-");

			vm.CalculatorButtonCommand.Execute(addBtn);
			vm.CalculatorButtonCommand.Execute(minusBtn);
			Assert.IsTrue(vm.EquationText == "-");
		}

		[Test]
		public void DirectPressSoundAppendToTheBuffer()
		{
			var vm = new  CalculatorViewModel();

			var oneBtn = vm.Buttons.FirstOrDefault(x => x.TitleText.ToLower() == "1");

			vm.CalculatorButtonCommand.Execute(oneBtn);
			Assert.IsTrue(vm.BufferText == "1");
		}

		[Test]
		public void DirectPressShouldClearEquationLineWhenTheEquationIsAConstant()
		{
			var vm = new  CalculatorViewModel();

			var oneBtn = vm.Buttons.FirstOrDefault(x => x.TitleText.ToLower() == "1");
			var equalsBtn = vm.Buttons.FirstOrDefault(x => x.TitleText.ToLower() == "=");

			vm.CalculatorButtonCommand.Execute(oneBtn);
			vm.CalculatorButtonCommand.Execute(equalsBtn);
			vm.CalculatorButtonCommand.Execute(oneBtn);
			Assert.IsTrue(String.IsNullOrEmpty(vm.EquationText));
		}

		[Test]
		public void ExpressionPressShouldSendToEquationLine()
		{
			var vm = new  CalculatorViewModel();

			var oneBtn = vm.Buttons.FirstOrDefault(x => x.TitleText.ToLower() == "1");
			var sinBtn = vm.Buttons.FirstOrDefault(x => x.TitleText.ToLower() == "sin");

			vm.CalculatorButtonCommand.Execute(oneBtn);
			vm.CalculatorButtonCommand.Execute(sinBtn);
			Assert.IsTrue(vm.EquationText != String.Empty);
		}

		[Test]
		public void ExpressionPressShouldDoNothingWhenBufferIsEmpty()
		{
			var vm = new  CalculatorViewModel();

			var sinBtn = vm.Buttons.FirstOrDefault(x => x.TitleText.ToLower() == "sin");

			vm.CalculatorButtonCommand.Execute(sinBtn);
			Assert.IsTrue(String.IsNullOrEmpty(vm.EquationText));
		}

		[Test]
		public void InlinePressShouldReplaceBuffer()
		{
			var vm = new  CalculatorViewModel();

			var oneBtn = vm.Buttons.FirstOrDefault(x => x.TitleText.ToLower() == "1");
			var plusMinusBtn = vm.Buttons.FirstOrDefault(x => x.TitleText.ToLower() == "±");

			vm.CalculatorButtonCommand.Execute(oneBtn);
			var oldBuffer = vm.BufferText;
			vm.CalculatorButtonCommand.Execute(plusMinusBtn);
			Assert.IsTrue(oldBuffer != vm.BufferText);
		}
	}
}
﻿using System;
using Ignite.Abacus.Mobile.Interfaces;

namespace Ignite.Abacus.iOS.Services
{
	public class ConsoleService : IConsoleService
	{
		public void WriteLine(string value, params object[] replacements)
		{ Console.WriteLine(value, replacements); }

		public void WriteLine(object value)
		{ Console.WriteLine(value); }

		public void Write(string value, params object[] replacements)
		{ Console.Write(value, replacements); }

		public void Write(object value)
		{ Console.Write(value); }
	}
}
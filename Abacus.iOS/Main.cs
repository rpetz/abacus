﻿using MonoTouch.UIKit;

namespace Ignite.Abacus.iOS
{
	public class Application
	{
		static void Main (string[] args)
		{ 
			UIApplication.Main (args, null, "AppDelegate"); 
		}
	}
}

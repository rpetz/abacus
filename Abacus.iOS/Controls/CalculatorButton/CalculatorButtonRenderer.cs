﻿using System.Drawing;
using Ignite.Abacus.iOS.Controls;
using MonoTouch.UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Labs.Controls;
using Xamarin.Forms.Platform.iOS;
using MonoTouch.CoreGraphics;

[assembly: ExportRenderer (typeof (Ignite.Abacus.Mobile.Controls.CalculatorButton), typeof (CalculatorButtonRenderer))]
namespace Ignite.Abacus.iOS.Controls
{
	public class CalculatorButtonRenderer : ButtonRenderer
	{
		private readonly UIColor _normalGradientDark = UIColor.FromRGBA(0.778f, 0.778f, 0.778f, 1.000f);
		private readonly UIColor _normalGradientMiddle = UIColor.FromRGBA(0.953f, 0.953f, 0.953f, 1.000f);

		private readonly CGColor[] _normalGradientColors;
		private readonly float[] _normalGradientLocations = { 0.0f, 0.49f, 0.51f, 1.0f };

		public CalculatorButtonRenderer() {
			_normalGradientColors = new[] {
				_normalGradientDark.CGColor,
				_normalGradientDark.BlendedColor (0.5f, _normalGradientMiddle).CGColor,
				_normalGradientMiddle.CGColor,
				UIColor.White.CGColor
			};
		}

		protected override void Dispose (bool disposing)
		{
			if (disposing && Control != null) {
				// Add any disposal logic here.
			}
			base.Dispose (disposing);
		}

		/// <summary>
		/// Gets the underlying element typed as an <see cref="ImageButton"/>.
		/// </summary>
		private Mobile.Controls.CalculatorButton CalculatorButton
		{
			get { return (Mobile.Controls.CalculatorButton)Element; }
		}

		public override void Draw (RectangleF rect)
		{
			using (var colorSpace = CGColorSpace.CreateDeviceRGB ())
			using (var context = UIGraphics.GetCurrentContext()) 
			{
				var normalGradient = new CGGradient (colorSpace, _normalGradientColors, _normalGradientLocations);

				var buttonRectangleRect = new RectangleF (rect.GetMinX (), rect.GetMinY (), rect.Width, rect.Height);
				var buttonRectanglePath = UIBezierPath.FromRect (buttonRectangleRect);
				context.SaveState ();
				buttonRectanglePath.AddClip ();
				context.DrawLinearGradient (normalGradient,
					new PointF (buttonRectangleRect.GetMidX (), buttonRectangleRect.GetMaxY ()),
					new PointF (buttonRectangleRect.GetMidX (), buttonRectangleRect.GetMinY ()),
					0);
				context.RestoreState ();
				UIColor.LightGray.SetStroke ();
				buttonRectanglePath.LineWidth = 2.0f;
				buttonRectanglePath.Stroke ();

				CalculatorButton.TextColor = UIColor.DarkGray.ToXamarinColor ();
			}
		}
	}
}


﻿using Ignite.Abacus.iOS.Controls;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using System.Drawing;
using MonoTouch.UIKit;
using MonoTouch.CoreGraphics;

[assembly: ExportRenderer (typeof (Ignite.Abacus.Mobile.Controls.CalculatorScreen), typeof (CalculatorScreenRenderer))]
namespace Ignite.Abacus.iOS.Controls
{
	public class CalculatorScreenRenderer : BoxRenderer
	{
		private readonly UIColor _screenGradientDark = UIColor.FromRGBA(0.93f, 0.93f, 1.0f, 1.0f);
		private readonly UIColor _screenGradientLight = UIColor.FromRGBA(1.0f, 1.0f, 1.0f, 1.0f);
		private readonly UIColor _shadow = UIColor.FromRGBA (0.53f, 0.53f, 1.0f, 0.45f);

		private readonly CGColor[] _screenGradientColors;
		private readonly float[] _screenGradientLocations = { 0.0f, 1.0f };
		private readonly SizeF _shadowOffset = new SizeF(0.1f, -0.1f);
		private const float ShadowBlurRadius = 10.0f;

		public CalculatorScreenRenderer ()
		{
			_screenGradientColors = new[] { _screenGradientDark.CGColor, _screenGradientLight.CGColor };
		}

		public override void Draw (RectangleF rect)
		{
			var colorSpace = CGColorSpace.CreateDeviceRGB();
			var context = UIGraphics.GetCurrentContext();

			var screenGradient = new CGGradient(colorSpace, _screenGradientColors, _screenGradientLocations);

			var rectangleRect = new RectangleF(rect.GetMinX(), rect.GetMinY(), rect.Width, rect.Height);
			var rectanglePath = UIBezierPath.FromRoundedRect(rectangleRect, 3.0f);
			context.SaveState();
			rectanglePath.AddClip();
			context.DrawLinearGradient(screenGradient,
				new PointF(rectangleRect.GetMidX(), rectangleRect.GetMaxY()),
				new PointF(rectangleRect.GetMidX(), rectangleRect.GetMinY()),
				0);
			context.RestoreState();

			context.SaveState();
			context.ClipToRect(rectanglePath.Bounds);
			context.SetShadow(new SizeF(0, 0), 0);
			context.SetAlpha(_shadow.CGColor.Alpha);
			context.BeginTransparencyLayer();
			{
				var opaqueShadow = new CGColor(_shadow.CGColor, 1.0f);
				context.SetShadowWithColor(_shadowOffset, ShadowBlurRadius, opaqueShadow);
				context.SetBlendMode(CGBlendMode.SourceOut);
				context.BeginTransparencyLayer();

				context.SetFillColor(opaqueShadow);
				rectanglePath.Fill();

				context.EndTransparencyLayer();
			}
			context.EndTransparencyLayer();
			context.RestoreState();

			UIColor.FromRGBA (0.0f, 0.0f, 0.0f, 0.25f).SetStroke ();
			rectanglePath.LineWidth = 2.0f;
			rectanglePath.Stroke();
		}
	}
}


﻿using MonoTouch.UIKit;

namespace Ignite.Abacus.iOS.Controls
{
	public static class Extensions
	{
		public static UIColor BlendedColor(this UIColor color1, float fraction, UIColor color2)
		{
			var rgba1 = new float[4];
			var rgba2 = new float[4];

			color1.GetRGBA(out rgba1 [0], out rgba1 [1], out rgba1 [2], out rgba1 [3]);
			color2.GetRGBA(out rgba2 [0], out rgba2 [1], out rgba2 [2], out rgba2 [3]);

			return new UIColor (
				rgba1 [0] * (1 - fraction) + rgba2 [0] * fraction,
				rgba1 [1] * (1 - fraction) + rgba2 [1] * fraction,
				rgba1 [2] * (1 - fraction) + rgba2 [2] * fraction,
				rgba1 [3] * (1 - fraction) + rgba2 [3] * fraction);
		}

		public static Xamarin.Forms.Color ToXamarinColor(this UIColor color)
		{
			float darkGrayR;
			float darkGrayG;
			float darkGrayB;
			float darkGrayA;
			UIColor.DarkGray.GetRGBA (out darkGrayR, out darkGrayG, out darkGrayB, out darkGrayA);

			return new Xamarin.Forms.Color (darkGrayR, darkGrayG, darkGrayB, darkGrayA);
		}
	}
}


﻿using MonoTouch.UIKit;
using System.Drawing;

namespace Ignite.Abacus.iOS.Controls
{
	public class NoCaretField : UITextField
	{
		public NoCaretField() : base(default(RectangleF))
		{ }

		public override RectangleF GetCaretRectForPosition(UITextPosition position)
		{
			return default(RectangleF);
		}
	}
}


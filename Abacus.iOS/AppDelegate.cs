﻿using Ignite.Abacus.iOS.Services;
using Ignite.Abacus.Mobile;
using Ignite.Abacus.Mobile.Interfaces;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using ReactiveUI;
using Splat;
using Xamarin.Forms;
using Xamarin.Forms.Labs.iOS;
using System;
using System.Collections.Generic;

namespace Ignite.Abacus.iOS
{
	[Register ("AppDelegate")]
	public partial class AppDelegate : XFormsApplicationDelegate
	{
		UIWindow _window;
		AutoSuspendHelper _suspendHelper;
		UIViewController vc;

		public AppDelegate()
		{
			App.BindDependencies ();
			Locator.CurrentMutable.Register(() => new ConsoleService(), typeof(IConsoleService));

			RxApp.SuspensionHost.CreateNewAppState = () => new AppBootstrapper ();
		}

		public override bool FinishedLaunching(UIApplication app, NSDictionary options)
		{
			Forms.Init();
			RxApp.SuspensionHost.SetupDefaultSuspendResume();

			// http://forums.xamarin.com/discussion/21148/calabash-and-xamarin-forms-what-am-i-missing
			Forms.ViewInitialized += (sender, e) => {
				// http://developer.xamarin.com/recipes/testcloud/set-accessibilityidentifier-ios/
				if (null != e.View.StyleId)
					e.NativeView.AccessibilityIdentifier = e.View.StyleId;
			};

			_suspendHelper = new AutoSuspendHelper(this);
			_suspendHelper.FinishedLaunching(app, options);

			_window = new UIWindow (UIScreen.MainScreen.Bounds);
			var bootstrapper = RxApp.SuspensionHost.GetAppState<AppBootstrapper> ();

			_window.RootViewController = bootstrapper.CreateMainPage ().CreateViewController ();
			_window.MakeKeyAndVisible ();

			#if DEBUG
			Xamarin.Calabash.Start();
			#endif

			return true;
		}

		public override void DidEnterBackground(UIApplication application)
		{ _suspendHelper.DidEnterBackground(application); }

		public override void OnActivated(UIApplication application)
		{ _suspendHelper.OnActivated(application); }
	}
}

